#pragma once

/*
	以文本的形式打开文件进行操作，遇见0XFF会结束
	而以二进制的方式打开文件就没有任何问题
*/
#include "HuffermanTree.h"
#include <string>
#include <cstdio>
#include <iostream>
#include <vector>	//test

//使用unsigned char表示ASCII码的表示范围，可以包括汉字的ASCII码(>127)
typedef unsigned char UChar;

using std::string;
using std::to_string;	//to_string在std命名空间下
using std::cout;
using std::endl;
using std::vector;	//test	

//字节信息
struct ByteInfo
{
	UChar _ch;		//对应的字节
	string strCode;	//字节对应的哈夫曼编码
	int _cnt;		//该字节出现的次数

	ByteInfo(int cnt = 0)
		:_cnt(cnt)
	{}

	ByteInfo operator+(const ByteInfo obj) const
	{
		ByteInfo newInfo(_cnt + obj._cnt);
		return newInfo;
	}

	bool operator>(const ByteInfo obj) const
	{
		return _cnt > obj._cnt;
	}

	bool operator==(const ByteInfo obj) const
	{
		return _cnt == obj._cnt;
	}
	bool operator!=(const ByteInfo obj) const
	{
		return _cnt != obj._cnt;
	}
};

class Compress
{
public:
	Compress()
	{
		//初始化每个字节的信息
		for (int i = 0; i < 256; ++i)
		{
			bytesInfo[i]._ch = i;
		}
	}

	//压缩
	void compress(const string& inputPath, const string& outputPath)
	{
		//1.读取文件的每个字节的出现次数
		FILE* inputfp = fopen(inputPath.c_str(), "rb");
		if (!inputfp)
		{
			cout << inputPath << "打开失败" << endl;
			return;
		}

		UChar buf[1024] = { 0 };	//读取数据的缓冲区
		while (1)
		{
			int rdsize = fread(buf, sizeof(UChar), 1024, inputfp);

			//读取数据失败就说明次数已经统计结束
			if (rdsize == 0)
				break;

			for (int i = 0; i < rdsize; ++i)
			{
				++bytesInfo[buf[i]]._cnt;
			}
		}

		//2.构建哈夫曼树
		HuffermanTree<ByteInfo> ht;
		ByteInfo invalidByte;
		ht.createTree(bytesInfo, 256, invalidByte);

		//3.生成哈夫曼编码
		HuffmanTreeNode<ByteInfo>* root = ht.getRoot();
		string curStr;
		createHuffermanCode(root, curStr);

		//4.给压缩文件写入解压缩的信息，并将源文件的每个字节替换为对应的哈夫曼编码
		fseek(inputfp, SEEK_SET, 0);	//让读取原文件的文件指针归位到头部
		FILE* outputfp = fopen(outputPath.c_str(), "wb");
		if (!outputfp)
		{
			cout << outputfp << "open failed!" << endl;
			return;
		}

		//1)给压缩文件写入解压缩的信息
		writeHead(outputfp, inputPath);

		//2).将源文件的每个字节替换为对应的哈夫曼编码
		UChar ch = 0;	//辅助字节
		int bitcnt = 0;	//字节中有效的比特位个数

		while (1)
		{
			int rdsize = fread(buf, sizeof(char), 1024, inputfp);
			if (rdsize == 0)
				break;

			for (int i = 0; i < rdsize; ++i)
			{
				//获取到该字节对应的哈夫曼码
				string str_code = bytesInfo[buf[i]].strCode;
				for (size_t j = 0; j < str_code.size(); ++j)
				{
					ch <<= 1;

					if (str_code[j] == '1')
						ch |= 1;

					bitcnt++;

					//一个字节满了就将其写入输出文件
					if (bitcnt == 8)
					{
						fputc(ch, outputfp);
						bitcnt = 0;
					}
				}
			}
		}

		//将不满一个字节的数据写入
		if (bitcnt > 0 && bitcnt < 8)
		{
			ch <<= (8 - bitcnt);
			fputc(ch, outputfp);
		}

		fclose(outputfp);
		fclose(inputfp);
	}

	//解压缩
	void unCompress(const string& inputPath, const string& outputFileName)
	{
		//1.读取压缩文件，并构建哈夫曼树
		FILE* fp_rd = fopen(inputPath.c_str(), "rb");
		if (!fp_rd)
			return;

		string extension;	//扩展名
		GetLine(fp_rd, extension);

		//解压缩后的文件名
		string outputFile = outputFileName + extension;

		//获取键值对个数
		string lineCnt_str;
		GetLine(fp_rd, lineCnt_str);

		int lineCnt = stoi(lineCnt_str);
		string kv_content;	//kv键值对信息

		//将键值对信息存入bytesInfo数组
		for (int i = 0; i < lineCnt; ++i)
		{
			//获取一行数据
			GetLine(fp_rd, kv_content);

			//读取到了一个\n,注意此处的\n不是每一行末尾的结束标志，而是文件中的字符
			//继续读取这一行的\n后面的内容
			if (kv_content == "")
			{
				kv_content += "\n";
				GetLine(fp_rd, kv_content);
			}
			
			UChar key = kv_content[0];
			int value = atoi(kv_content.substr(2).c_str());
			bytesInfo[key]._cnt = value;
			kv_content = "";
		}

		//构建哈夫曼树
		HuffermanTree<ByteInfo> ht;
		ByteInfo invalidByte;
		ht.createTree(bytesInfo, 256, invalidByte);

		//2.根据文件内容还原原文件
		FILE* outputFp = fopen(outputFile.c_str(), "wb");

		UChar buf[1024] = { 0 };
		//从根节点向下遍历，0遍历左孩子，1遍历右孩子，直到叶子节点
		HuffmanTreeNode<ByteInfo>* root = ht.getRoot();		
		HuffmanTreeNode<ByteInfo>* curNode = root;

		int fileSize = ht.getRoot()->_weight._cnt;	//压缩前实际文件的字节数
		int curWRsize = 0;	//写入文件的字节数
		bool flag = false;	//标记是否已经解压缩结束

		while (1)
		{
			int rdsize = fread(buf, sizeof(char), 1024, fp_rd);
			if (rdsize == 0)
				break;

			for (int i = 0; i < rdsize; ++i)
			{
				UChar ch = buf[i];
				int bitCnt = 0;
				while (bitCnt < 8)
				{
					//解析每个字节
					if (ch & 0x80)
					{
						curNode = curNode->_right;
					}
					else
					{
						curNode = curNode->_left;
					}
					//走到了哈夫曼树的叶子节点，将叶子结点的字节写入输出文件
					if (curNode->_left == nullptr && curNode->_right == nullptr)
					{
						fwrite(&(curNode->_weight._ch), sizeof(char), 1, outputFp);
						curNode = root;
						curWRsize++;
						//写入的数据和源文件大小相同，不再写入文件
						if (curWRsize == fileSize)
						{
							flag = true;
							break;
						}
					}
					ch <<= 1;
					bitCnt++;
				}
			}
			if (flag)
				break;
		}

		fclose(outputFp);
		fclose(fp_rd);
	}

private:
	//为每个有效节点生成哈夫曼编码
	void createHuffermanCode(HuffmanTreeNode<ByteInfo>* root, string curStr)
	{
		if (root == nullptr)
			return;

		if (root->_left == nullptr && root->_right == nullptr)
		{
			//走到叶节点，拿到叶节点对应的字节，并修改哈夫曼编码
			ByteInfo& byteInfo = bytesInfo[root->_weight._ch];
			byteInfo.strCode = curStr;
			return;
		}

		createHuffermanCode(root->_left, curStr + '0');
		createHuffermanCode(root->_right, curStr + '1');
	}

	//给压缩文件写入头部信息
	void writeHead(FILE* outputFp, const string& inputFile)
	{
		//获取文件扩展名
		string expandName = inputFile.substr(inputFile.find('.'));
		expandName += "\n";
		fwrite(expandName.c_str(), sizeof(char), expandName.size(), outputFp);

		//获取词频信息个数和构建词频信息
		int lineCnt = 0;
		string wordCnt;
		for (int i = 0; i < 256; ++i)
		{
			if (bytesInfo[i]._cnt > 0)
			{
				wordCnt += bytesInfo[i]._ch;
				wordCnt += ":";
				wordCnt += to_string(bytesInfo[i]._cnt);
				wordCnt += "\n";
				lineCnt++;
			}
		}
		string str_linecnt = to_string(lineCnt);
		str_linecnt += "\n";
		fwrite(str_linecnt.c_str(), sizeof(char), str_linecnt.size(), outputFp);
		fwrite(wordCnt.c_str(), sizeof(char), wordCnt.size(), outputFp);
	}

	//读取文件的一行
	void GetLine(FILE* fp, string& str)
	{
		UChar ch;
		while (!feof(fp))
		{
			//每次读取一个字符，遇见换行符停止
			fread(&ch, sizeof(char), 1, fp);
			if (ch == '\n')
			{
				break;
			}
			else
			{
				str += ch;
			}
		}
	}
private:
	ByteInfo bytesInfo[256];	//哈希结构存储每个字节信息,从ASCII值为0~255
};