#include "HuffermanTree.h"
#include "Compress.h"
#include <iostream>
#include <string>
#include <cstdlib>

using std::cin;
using std::cout;
using std::endl;
using std::string;

void menu()
{
	cout << "-------------  文件压缩系统    ----------------" << endl;
	cout << "-------------   0:退出         ----------------" << endl;
	cout << "-------------   1:压缩文件     ----------------" << endl;
	cout << "-------------   2：解压缩文件  ----------------" << endl;
	cout << "-------------  请输入您的操作: ----------------" << endl;
}
int main()
{
	while (1)
	{
		menu();
		int choice;
		cin >> choice;

		if (choice == 0)
			break;
		else if (choice == 1)
		{
			string inputFilePath;
			string outputFileName;
			cout << "请输入要压缩的文件名:";
			cin >> inputFilePath;
			cout << "请输入压缩文件名：";
			cin >> outputFileName;

			Compress obj;
			cout << "开始压缩" << endl;
			obj.compress(inputFilePath, outputFileName);
			cout << "压缩成功!" << endl;
			system("pause");
			system("cls");
		}
		else if (choice == 2)
		{
			string inputFilePath;
			string outputFileName;
			cout << "请输入要解压缩的文件名:";
			cin >> inputFilePath;
			cout << "请输入解压缩后的文件名：";
			cin >> outputFileName;

			Compress obj;
			cout << "开始解压缩" << endl;
			obj.unCompress(inputFilePath, outputFileName);
			cout << "解压缩压缩成功!" << endl;
			system("pause");
			system("cls");
		}
		else
		{
			cout << "输入选项不存在，请重新输入!!" << endl;
			system("cls");
		}
	}

	cout << "欢迎您再次使用文件压缩系统！" << endl;
	return 0;
}