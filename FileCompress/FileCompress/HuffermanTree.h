#pragma once

#include <queue>
#include <vector>

using std::priority_queue;
using std::vector;

//哈夫曼树节点
template<class T>
struct HuffmanTreeNode
{
	HuffmanTreeNode(const T weight)
		:_left(nullptr)
		, _right(nullptr)
		, _weight(weight)
	{}
	HuffmanTreeNode<T>* _left;	//左孩子
	HuffmanTreeNode<T>* _right;	//右孩子
	T _weight;	//当前节点的权值
};

//权值比较仿函数
template<class T>
struct cmp
{
	typedef HuffmanTreeNode<T> Node;
	bool operator()(Node* left, Node* right)
	{
		return left->_weight > right->_weight;
	}
};

//哈夫曼树
template<class T>
class HuffermanTree
{
	typedef HuffmanTreeNode<T> Node;
public:
	HuffermanTree()
		:_root(nullptr)
	{}

	void createTree(const T arr[], int sz, const T invalid)
	{
		//1.将所有节点放入一个森林
		priority_queue<Node*, vector<Node*>, cmp<T>> q;
		for (int i = 0; i < sz; ++i)
		{
			if (arr[i] != invalid)
				q.push(new Node(arr[i]));
		}

		//2.每次从森林中拿出最小的两个节点形成一个新节点
		//直到森林中只有一个节点
		while (!q.empty())
		{
			Node* left = q.top();
			q.pop();
			Node* right = q.top();
			q.pop();

			Node* newNode = new Node(left->_weight + right->_weight);
			q.push(newNode);
			newNode->_left = left;
			newNode->_right = right;

			if (q.size() == 1)
			{
				_root = q.top();
				break;
			}
		}
	}

	Node* getRoot()
	{
		return _root;
	}

	~HuffermanTree()
	{
		Destroy(_root);
	}

	void Destroy(Node* root)
	{
		if (root)
		{
			Destroy(root->_left);
			Destroy(root->_right);
			delete root;
		}
	}

	HuffermanTree(const HuffermanTree<T>& obj) = delete;
	HuffermanTree& operator=(const HuffermanTree<T>& obj) = delete;
private:
	Node* _root;
};