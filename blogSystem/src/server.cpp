#include "db.hpp"
#include "httplib.h"
#include <cstdio>

using namespace httplib;

//创建两个全局变量,来对数据库中的两个表进行操作
blog_system::TableBlog *table_blog;
blog_system::TableTag *table_tag;

////////////////////////////////////////////////////////////////////////
//业务逻辑处理模块
////////////////////////////////////////////////////////////////////////

//博客类对应业务处理操作
void InsertBlog(const Request& req, Response& rsp)
{
	//从请求中取出正文，正文的格式为json
	//将json格式字符串反序列化，得到博客的各项信息
	Json::Reader reader;
	Json::Value blog;
	Json::FastWriter writer;
	Json::Value errmsg;//错误信息

	bool ret=reader.parse(req.body,blog);
	if(ret == false){
		printf("InsertBlog parse blog json failed!\n");
		rsp.status=400;//服务器无法处理请求
		errmsg["ok"]="false";
		errmsg["reason"]="parse blog json failed!";
		rsp.set_content(writer.write(errmsg),"application/json");
		return;
	}
	//将得到的博客信息插入到数据库
	ret=table_blog->Insert(blog);
	printf("after insert blog\n");
	if(ret == false){
		rsp.status=500;//服务器处理请求出错
		errmsg["ok"]="false";
		errmsg["reason"]="insert into database failed!";
		rsp.set_content(writer.write(errmsg),"application/json");
		return;
	}
	rsp.status=200;
	return;
}

void DeleteBlog(const Request& req, Response& rsp)
{
	Json::Value errmsg;//错误信息
	Json::FastWriter writer;
	//若请求为/blog/123 则req.matches(0)为/blog/123,req.matcher(1)为123
	int tag_id=std::stoi(req.matches[1]);
	bool ret=table_blog->Delete(tag_id);
	if(ret == false){
		printf("DelteBlog delete data from database failed!\n");
		rsp.status=500;//服务器处理请求出错
		errmsg["ok"]="false";
		errmsg["reason"]="delete data from database failed!";
		rsp.set_content(writer.write(errmsg),"application/json");
		return;
	}
	rsp.status=200;

	return;
}

void UpdateBlog(const Request& req, Response& rsp)
{
	int blog_id=std::stoi(req.matches[1]);
	Json::Value blog;
	Json::Value errmsg;//错误信息
	Json::FastWriter writer;
	Json::Reader reader;
	//将json格式的字符串反序列化存放到blog对象中
	bool ret=reader.parse(req.body,blog);
	if(ret == false){
		printf("UpdateBlog parse blog json failed!\n");
		rsp.status=400;//服务器无法处理请求
		errmsg["ok"]="false";
		errmsg["reason"]="parse blog json failed!";
		rsp.set_content(writer.write(errmsg),"application/json");
		return;
	}
	//重新为id赋值防止请求的id不存在
	blog["id"]=blog_id;
	ret=table_blog->Update(blog);
	if(ret == false){
		printf("UpdateBlog update data from database failed!\n");
		rsp.status=500;//服务器处理请求出错
		errmsg["ok"]="false";
		errmsg["reason"]="update data from database failed!";
		rsp.set_content(writer.write(errmsg),"application/json");
		return;
	}
	rsp.status=200;

	return;
}
void GetAllBlog(const Request& req, Response& rsp)
{
	Json::Value errmsg;//错误信息
	Json::Value blogs;
	Json::FastWriter writer;

	//从数据库中取出博客列表信息
	bool ret=table_blog->GetAll(&blogs);
	if(ret == false){
		printf("GetAllBlog select data from database failed!\n");
		rsp.status=500;//服务器处理请求出错
		errmsg["ok"]="false";
		errmsg["reason"]="select data from database failed!";
		rsp.set_content(writer.write(errmsg),"application/json");
		return;
	}

	//将数据进行json序列化，添加到rsp正文中
	rsp.set_content(writer.write(blogs),"application/json");
	rsp.status=200;

	return;
}

void GetOneBlog(const Request& req, Response& rsp)
{
	int blog_id=std::stoi(req.matches[1]);
	Json::Value errmsg;//错误信息
	Json::Value blog;
	Json::FastWriter writer;
	blog["id"]=blog_id;

	//从数据库中取出博客信息
	bool ret=table_blog->GetOne(&blog);
	if(ret == false){
		printf("GetOneBlog select data from database failed!\n");
		rsp.status=500;//服务器处理请求出错
		errmsg["ok"]="false";
		errmsg["reason"]="select data from database failed!";
		rsp.set_content(writer.write(errmsg),"application/json");
		return;
	}

	//将数据进行json序列化，添加到rsp正文中
	rsp.set_content(writer.write(blog),"application/json");
	rsp.status=200;

	return;
}

//标签类对应业务处理操作
void InsertTag(const Request& req, Response& rsp)
{
	//从请求中取出正文，正文的格式为json
	//将json格式字符串反序列化，得到博客的各项信息
	Json::Reader reader;
	Json::Value tag;
	Json::FastWriter writer;
	Json::Value errmsg;//错误信息

	bool ret=reader.parse(req.body, tag);
	if(ret == false){
		printf("InsertTag parse tag json failed!\n");
		rsp.status=400;//服务器无法处理请求
		errmsg["ok"]="false";
		errmsg["reason"]="parse tag json failed!";
		rsp.set_content(writer.write(errmsg),"application/json");
		return;
	}
	//将得到的博客信息插入到数据库
	ret=table_tag->Insert(tag);
	if(ret == false){
		printf("InsertTag insert into database failed!\n");
		rsp.status=500;//服务器处理请求出错
		errmsg["ok"]="false";
		errmsg["reason"]="insert into database failed!";
		rsp.set_content(writer.write(errmsg),"application/json");
		return;
	}
	rsp.status=200;

	return;
}

void DeleteTag(const Request& req, Response& rsp)
{
	Json::Value errmsg;//错误信息
	Json::FastWriter writer;
	//若请求为/tag/123 则req.matches(0)为/tag/123,req.matcher(1)为123
	int tag_id=std::stoi(req.matches[1]);
	bool ret=table_tag->Delete(tag_id);
	if(ret == false){
		printf("DeleteTag delete data from database failed!\n");
		rsp.status=500;//服务器处理请求出错
		errmsg["ok"]="false";
		errmsg["reason"]="delete data from database failed!";
		rsp.set_content(writer.write(errmsg),"application/json");
		return;
	}
	rsp.status=200;

	return;
}

void UpdateTag(const Request& req, Response& rsp){
	int tag_id=std::stoi(req.matches[1]);
	Json::Value tag;
	Json::Value errmsg;//错误信息
	Json::FastWriter writer;
	Json::Reader reader;
	//将json格式的字符串反序列化存放到tag对象中
	bool ret=reader.parse(req.body, tag);
	if(ret == false){
		printf("UpdateTag parse tag json failed!\n");
		rsp.status=400;//服务器无法处理请求
		errmsg["ok"]="false";
		errmsg["reason"]="parse tag json failed!";
		rsp.set_content(writer.write(errmsg),"application/json");
		return;
	}
	//重新为id赋值防止请求的id不存在
	tag["id"]=tag_id;
	ret=table_tag->Update(tag);
	if(ret == false){
		printf("UpdateTag update data from database failed!\n");
		rsp.status=500;//服务器处理请求出错
		errmsg["ok"]="false";
		errmsg["reason"]="update data from database failed!";
		rsp.set_content(writer.write(errmsg),"application/json");
		return;
	}
	rsp.status=200;

	return;
}

void GetAllTag(const Request& req, Response& rsp)
{
	Json::Value errmsg;//错误信息
	Json::Value tags;
	Json::FastWriter writer;
	//从数据库中取出博客列表信息
	bool ret=table_tag->GetAll(&tags);
	if(ret == false){
		printf("GetAllTag select data from database failed!\n");
		rsp.status=500;//服务器处理请求出错
		errmsg["ok"]="false";
		errmsg["reason"]="select data from database failed!";
		rsp.set_content(writer.write(errmsg),"application/json");
		return;
	}

	//将数据进行json序列化，添加到rsp正文中
	rsp.set_content(writer.write(tags),"application/json");
	rsp.status=200;

	return;
}

void GetOneTag(const Request& req, Response& rsp)
{
	int tag_id=std::stoi(req.matches[1]);
	Json::Value errmsg;//错误信息
	Json::Value tag;
	Json::FastWriter writer;
	tag["id"]=tag_id;

	//从数据库中取出博客列表信息
	bool ret=table_tag->GetOne(&tag);
	if(ret == false){
		printf("GetOneTag select data from database failed!\n");
		rsp.status=500;//服务器处理请求出错
		errmsg["ok"]="false";
		errmsg["reason"]="select data from database failed!";
		rsp.set_content(writer.write(errmsg),"application/json");
		return;
	}

	//将数据进行json序列化，添加到rsp正文中
	rsp.set_content(writer.write(tag),"application/json");
	rsp.status=200;

	return;
}

int main()
{
	//初始化访问数据库的两个对象
	MYSQL* mysql=blog_system::MysqlInit();
	table_blog=new blog_system::TableBlog(mysql);
	table_tag=new blog_system::TableTag(mysql);

	//搭建http服务器
	Server server;
	//设置相对根目录，当客户端请求静态资源文件时，httplib会直接根据路径读取文件资源进行响应
	//并且在请求时会自动加上index.html
	server.set_base_dir("./www");
	
	/*********************博客信息的增删查改*********************/
	//增加
	server.Post("/blog",InsertBlog);
	//删除
	//R"()":去除括号中所有字符的特殊属性-----C++11
	//正则表达式:  \d:匹配单个数字字符 +:匹配前边的字符一次或多次  ()临时保存匹配的数据
	//R"(/blog/(\d+))"表示匹配以/blog/开头,后面跟1个或多个数字字符的字符串格式，并且临时保存后面的数字
	server.Delete(R"(/blog/(\d+))",DeleteBlog);
	//修改
	server.Put(R"(/blog/(\d+))",UpdateBlog);
	//查询所有博客
	server.Get("/blog",GetAllBlog);
	//查询单个博客
	server.Get(R"(/blog/(\d+))",GetOneBlog);

	/******************标签信息的增删查改************************/
	//增加
	server.Post("/tag",InsertTag);
	//删除
	//R"()":去除括号中所有字符的特殊属性-----C++11
	//正则表达式:  \d:匹配单个数字字符 +:匹配前边的字符一次或多次  ()临时保存匹配的数据
	//R"(/tag/(\d+))"表示匹配以/tag/开头,后面跟1个或多个数字字符的字符串格式，并且临时保存后面的数字
	server.Delete(R"(/tag/(\d+))",DeleteTag);
	//修改
	server.Put(R"(/tag/(\d+))",UpdateTag);
	//查询所有博客
	server.Get("/tag",GetAllTag);
	//查询单个博客
	server.Get(R"(/tag/(\d+))",GetOneTag);

	//服务器开始监听
	server.listen("0.0.0.0",9000);

	//关闭mysql连接
	blog_system::MysqlRelease(mysql);
	return 0;
}
