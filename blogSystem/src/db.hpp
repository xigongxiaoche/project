#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <mysql/mysql.h>
#include <mutex>
#include <jsoncpp/json/json.h>

#define HostName "127.0.0.1"
#define UserName "root"
#define Passwd "123"
#define MysqlName "blogSystem"

//封装两个类对两张表table_blog,table_tag 进行操作

namespace blog_system{
	std::mutex g_mutex;//互斥锁

	//向外提供接口返回初始化完成的mysql句柄
	//连接服务器，选择数据库，设置字符集
	MYSQL* MysqlInit(){
		MYSQL* mysql=NULL;

		//1.初始化mysql句柄
		mysql=mysql_init(NULL);
		if(mysql == NULL)
		{
			printf("mysql init error\n");
			return NULL;
		}
		//2.连接服务器
		//mysql_real_connect(句柄，主机地址，用户名，密码，数据库名称，端口号，套接字，标志位)
		//端口号为0，默认3306
		//标志位通常置为0
		if(mysql_real_connect(mysql, HostName, UserName, Passwd,
					NULL, 0, NULL, 0) == NULL){
			printf("mysql connect error: %s\n",mysql_error(mysql));
			mysql_close(mysql);
			return NULL;
		}

		//3.设置字符集
		if(mysql_set_character_set(mysql,"utf8") != 0){
			printf("set character failed: %s\n",mysql_error(mysql));
			mysql_close(mysql);
			return NULL;
		}

		//4.选择数据库
		if(mysql_select_db(mysql,MysqlName) != 0){
			printf("select db error:%s\n",mysql_error(mysql));
			mysql_close(mysql);
			return NULL;
		}
		return mysql;
	}

	//销毁句柄
	void MysqlRelease(MYSQL* mysql){
		if(mysql)
			mysql_close(mysql);
		return;
	}

	//执行sql语句
	bool MysqlQuery(MYSQL* mysql,const char* sql){
		int ret = mysql_query(mysql, sql);
		if(ret != 0){
			printf("sql query[ %s ] error: %s\n", sql, mysql_error(mysql));
			return false;
		}
		return true;
	}


	//博客类，通过这个类对table_blog表进行操作
	class TableBlog{
		public:
			TableBlog(MYSQL* mysql) 
				:_mysql(mysql)
			{}

			//从blog中取出博客信息，组织sql语句，将数据插入数据库
			//表成员:id tag_id title content ctime
			bool Insert(Json::Value& blog){
#define INSERT_BLOG "insert into table_blog values(null, %d,'%s','%s',now());"
				int len=blog["content"].asString().size() + 4096;
				char* tmp=(char*)malloc(sizeof(char) * len);
				sprintf(tmp, INSERT_BLOG, blog["tag_id"].asInt(), 
						blog["title"].asCString(),
						blog["content"].asCString());
				bool ret = MysqlQuery(_mysql, tmp);
				free(tmp);
				return ret;
			}

			//根据博客id删除博客
			bool Delete(int blog_id){
#define DELETE_BLOG "delete from table_blog where id =%d;"
				char tmp[1024]={0};
				sprintf(tmp, DELETE_BLOG, blog_id);
				return MysqlQuery(_mysql, tmp);
			}

			//从blog中取出博客信息，组织sql语句，更新数据库中的数据
			//blog是一个输入输出型参数
			bool Update(Json::Value& blog){
#define UPDATE_BLOG "update table_blog set tag_id= %d, title='%s', content='%s' where id=%d;"
				int len=blog["content"].asString().size() + 4096;
				char* tmp=(char*)malloc(sizeof(char) * len);
				sprintf(tmp, UPDATE_BLOG, blog["tag_id"].asInt(),
						blog["title"].asCString(),
						blog["content"].asCString(),
						blog["id"].asInt());
				bool ret = MysqlQuery(_mysql, tmp);
				free(tmp);
				return ret;	
			}

			//通过blogs返回所有的博客信息(以列表形式展示，不包含正文)
			bool GetAll(Json::Value* blogs){
#define GETALL_BLOG "select id,tag_id,title,ctime from table_blog;"
				g_mutex.lock();
				//执行查询语句
				bool ret=MysqlQuery(_mysql, GETALL_BLOG); //非原子操作
				if(ret == false){
					printf("get allBlogs query failed!\n");
					g_mutex.unlock();
					return false;
				}
				//保存结果集(非原子操作)
				MYSQL_RES* allRes=mysql_store_result(_mysql);
				g_mutex.unlock();
				if(allRes == NULL){
					printf("store query result failed: %s\n",mysql_error(_mysql));
					return false;
				}

				//遍历结果集
				int num_rows=mysql_num_rows(allRes);
				for(int i=0;i<num_rows;++i){
					MYSQL_ROW row=mysql_fetch_row(allRes);
					Json::Value blog;
					blog["id"]=std::stoi(row[0]);
					blog["tag_id"]=std::stoi(row[1]);
					blog["title"]=row[2];
					blog["ctime"]=row[3];
					blogs->append(blog);//添加json数组对象
				}
				//释放结果集
				mysql_free_result(allRes);
				return true;
			}

			//返回单个博客信息，包括正文
			//blog是一个输入输出型参数
			bool GetOne(Json::Value* blog){
#define GETONE_BLOG "select tag_id,title,content,ctime from table_blog where id=%d;"
				//执行查询语句
				char tmp[1024]={0};
				sprintf(tmp,GETONE_BLOG,(*blog)["id"].asInt());
				g_mutex.lock();
				bool ret=MysqlQuery(_mysql,tmp);//非原子操作
				if(ret == false){
					printf("getone blog query[%s] failed\n",tmp);
					g_mutex.unlock();
					return false;
				}

				//获取结果集(非原子操作)
				MYSQL_RES* res=mysql_store_result(_mysql);
				g_mutex.unlock();
				if(res == NULL){
					printf("getone blog:store result failed\n",mysql_error(_mysql));
					return false;
				}

				//遍历结果集
				int num_rows=mysql_num_rows(res);
				if(num_rows != 1){
					printf("num_rows is wrong\n");
					mysql_free_result(res);
					return false;
				}

				//存储结果
				MYSQL_ROW row=mysql_fetch_row(res);
				(*blog)["tag_id"]=std::stoi(row[0]);
				(*blog)["title"]=row[1];
				(*blog)["content"]=row[2];
				(*blog)["ctime"]=row[3];

				//释放结果集
				mysql_free_result(res);
				return true;
			}
		private:
			MYSQL* _mysql;
	};


	//标签类,通过这个类对标签表进行操作
	class TableTag{
		public:
		TableTag(MYSQL* mysql)
			:_mysql(mysql)
		{}

		//插入标签信息
		bool Insert(Json::Value& tag){
#define INSERT_TAG "insert into table_tag value(null,'%s');"
		char tmp[1024]={0};
		sprintf(tmp,INSERT_TAG,tag["name"].asCString());
		return MysqlQuery(_mysql,tmp);
		}
		
		//删除对应id的标签
		bool Delete(int tag_id){
#define DELETE_TAG "delete from table_tag where id = %d;"
		char tmp[1024]={0};
		sprintf(tmp,DELETE_TAG,tag_id);
		return MysqlQuery(_mysql,tmp);
		}
		
		//修改标签名称
		bool Update(Json::Value& tag){
#define UPDATE_TAG "update table_tag set name='%s' where id=%d;"
		char tmp[1024]={0};
		sprintf(tmp,UPDATE_TAG,tag["name"].asCString(),tag["id"].asInt());
		return MysqlQuery(_mysql,tmp);
		}

		//获取所有信息
		bool GetAll(Json::Value* tags){
#define GETALL_TAG "select id,name from table_tag;"
				g_mutex.lock();
				//执行查询语句
				bool ret=MysqlQuery(_mysql, GETALL_TAG); //非原子操作
				if(ret == false){
					printf("get allTags query failed!\n");
					g_mutex.unlock();
					return false;
				}
				//保存结果集(非原子操作)
				MYSQL_RES* allRes=mysql_store_result(_mysql);
				g_mutex.unlock();
				if(allRes == NULL){
					printf("store query result failed: %s\n",mysql_error(_mysql));
					return false;
				}

				//遍历结果集
				int num_rows=mysql_num_rows(allRes);
				for(int i=0;i<num_rows;++i){
					MYSQL_ROW row=mysql_fetch_row(allRes);
					Json::Value tag;
					tag["id"]=std::stoi(row[0]);
					tag["name"]=row[1];
					tags->append(tag);//添加json数组对象
				}
				//释放结果集
				mysql_free_result(allRes);
				return true;
		}
		
		//获取单个信息
		bool GetOne(Json::Value* tag){
#define GETONE_TAG "select name from table_tag where id = '%d';"
			char tmp[1024]={0};
			sprintf(tmp,GETONE_TAG,(*tag)["id"].asInt());
				g_mutex.lock();
				//执行查询语句
				bool ret=MysqlQuery(_mysql, tmp); //非原子操作
				if(ret == false){
					printf("get oneTags query failed!\n");
					g_mutex.unlock();
					return false;
				}
				//保存结果集(非原子操作)
				MYSQL_RES* allRes=mysql_store_result(_mysql);
				g_mutex.unlock();
				if(allRes == NULL){
					printf("store query result failed: %s\n",mysql_error(_mysql));
					return false;
				}

				//遍历结果集
				int num_rows=mysql_num_rows(allRes);
				if(num_rows != 1){
					printf("get store result num error:%s\n",mysql_error(_mysql));
					mysql_free_result(allRes);
					return false;
				}
			
				MYSQL_ROW row=mysql_fetch_row(allRes);
				(*tag)["name"]=row[0];
				//释放结果集
				mysql_free_result(allRes);
				return true;

		}

		private:
			MYSQL* _mysql;
	};

}
