drop database if exists blogSystem;
create database if not exists blogSystem;

use blogSystem;

drop table if exists table_tag;
create table if not exists table_tag(
	id int primary key auto_increment comment "标签id",
	name varchar(32) comment "标签名称"
);

drop table if exists table_blog;
create table if not exists table_blog(
	id int primary key auto_increment comment "博客id",
	tag_id int comment "博客所属标签id",
	title varchar(255) comment "博客标题",
	content text comment "博客内容",
	ctime datetime comment "博客的创建时间"
);
