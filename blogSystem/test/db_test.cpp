#include "../src/db.hpp"

int main(){
	MYSQL* mysql;
	mysql=blog_system::MysqlInit();

	blog_system::TableBlog table_blog(mysql);

///////////////////////////////////////////////////////////////////////////////////////////////////
// 博客表数据管理模块测试
///////////////////////////////////////////////////////////////////////////////////////////////////

	//插入
/*	
	Json::Value v;
	v["tag_id"]=2;
	v["title"]="C++类型转换";
	v["content"]="四种类型转换";
	table_blog.Insert(v);
*/	

	//删除:测试成功
	//table_blog.Delete(2);
	
	//查找所有
/*	
	Json::Value blogs;
	table_blog.GetAll(&blogs);
	Json::StyledWriter writer;
	std::cout << writer.write(blogs) << std::endl;
*/	

	//查找单个
	/*
	Json::Value blog;
	blog["id"]=8;
	table_blog.GetOne(&blog);
	Json::StyledWriter writer;
	std::cout << writer.write(blog) << std::endl;
	*/

	
	//修改
	/*
	Json::Value blog;
	blog["id"]=1;
	blog["title"]="update测试";
	blog["content"]="四种类型转换,static_cast,reinterpret_cast,const_cast,dynamic_cast";
	blog["tag_id"]=1024;
	table_blog.Update(blog);

	blog_system::MysqlRelease(mysql);
*/
///////////////////////////////////////////////////////////////////////////////////////////////////
// 标签表数据管理模块测试
///////////////////////////////////////////////////////////////////////////////////////////////////
	
	blog_system::TableTag table_tag(mysql);

	//插入
	/*
	Json::Value tag;
	tag["name"]="Linux";

	table_tag.Insert(tag);
	*/

	//删除
	//table_tag.Delete(4);
	
	//修改标签名
	/*
	Json::Value tag;
	tag["id"]=2;
	tag["name"]="Linux";

	table_tag.Update(tag);
	*/

	//查询所有标签
	/*
	Json::Value tags;
	table_tag.GetAll(&tags);

	Json::StyledWriter writer;
	std::cout<< writer.write(tags)<<std::endl;
	*/

	//查询单个标签
	
	Json::Value tag;
	tag["id"]=1;
	table_tag.GetOne(&tag);

	Json::StyledWriter writer;
	std::cout<< writer.write(tag)<<std::endl;

	blog_system::MysqlRelease(mysql);
	

	return 0;
}
