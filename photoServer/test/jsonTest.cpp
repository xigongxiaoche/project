#include<iostream>
#include<string>
#include<jsoncpp/json/json.h>

int main(int argc,char* argv[])
{
	std::string name="车晓鹏";
	int age=21;
	std::string sex="男";
	float math=99.9;
	float chinese=100;
	float english=120.0;

	Json::Value val;//定义Json::Value对象，将数据都添加到其中
	val["姓名"]=name;
	val["年龄"]=age;
	val["性别"]=sex;
	val["成绩"].append(math);
	val["成绩"].append(chinese);
	val["成绩"].append(english);

	//进行数据序列化
	Json::StyledWriter writer;
	std::string json_str=writer.write(val);
	std::cout<<json_str<<std::endl;

	//反序列化
	std::string str=R"({"name":"张三","age":28,"sex":"男","score":[98.2,99.1,89]})";
	Json::Reader reader;
	Json::Value root;
	bool ret=reader.parse(str,root);
	if(ret == false){
		return -1;
	}

	std::cout<<"姓名:"<<root["name"].asString()<<std::endl;
	std::cout<<"性别:"<<root["sex"].asCString()<<std::endl;
	std::cout<<"年龄:"<<root["age"].asInt()<<std::endl;
	int num=root["score"].size();
	for(int i=0;i<num;++i){
		std::cout<<"成绩:"<<root["score"][i].asFloat()<<std::endl;
	}

	return 0;
}
