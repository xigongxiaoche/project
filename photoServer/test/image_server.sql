drop database if exists image_server;
create database if not exists image_server;

use image_server;

create table if not exists table_image(
	id int primary key auto_increment,
	name varchar(32) comment "图片名称",
	fsize int comment "图片大小",
	fpath varchar(255) comment "文件存放路径名",
	furl varchar(255) comment "前端访问图片的url",
	fmd5 varchar(64) comment "文件的MD5值",
	ultime datetime comment "图片上传时间"
);

insert into table_image values 
(1,"车晓鹏",1234,"./image/1.jpg","./image/1.jpg","wigh3hrwepi",now()),
(2,"张三",1234,"./image/1.jpg","./image/1.jpg","wigh3hrwepi",now()),
(3,"李四",1234,"./image/1.jpg","./image/1.jpg","wigh3hrwepi",now()),
(4,"王麻子",1234,"./image/1.jpg","./image/1.jpg","wigh3hrwepi",now());
