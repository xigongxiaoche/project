#include"../db.hpp"
#include"../httplib.h"

//实例化一个数据库访问对象，实现对数据的增删查改操作
void db_test()
{
	MYSQL* mysql=image_server::MysqlInit();
	image_server::TableImage table_image(mysql);

/*
 	//插入
	Json::Value image;
	image["name"]="星期三";
	image["fpath"]="./ll/";
	image["furl"]="./rehgru/OOO";
	image["fmd5"]="gqrhetgq3gq43hgqorhge";
	image["fsize"]=1024;

	table_image.Insert(image);
*/

	table_image.Delete(5);
	
/*	
        //修改
	Json::Value image;
	image["name"]="王龙";
	table_image.Update(3,image);
*/


	//查找所有
	Json::Value image;
	table_image.GetAll(&image);
	Json::StyledWriter writer;
	std::cout<<writer.write(image)<<std::endl;//序列化
	
/*
	Json::Value image;
	table_image.GetOne(1,&image);
	Json::StyledWriter writer;
	std::cout<<writer.write(image)<<std::endl;//序列化
*/

	image_server::MysqlRelease(mysql);
}

//httpLib库的使用
void httpLibTest()
{
	httplib::Server srv;

	srv.Get("/",[](const httplib::Request& req,httplib::Response& res){
			std::string body="<html><body><h1>nice day</h1></body></html>";
			res.set_content(body,"text/html");
		});
	
	srv.listen("0.0.0.0",9000);
}
int main()
{
	db_test();
	//httpLibTest();
	return 0;
}
