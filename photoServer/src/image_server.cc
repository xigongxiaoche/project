#include"httplib.h"
#include"db.hpp"
#include<openssl/md5.h>

#define WWW_ROOT "./www"
#define IMAGE_PATH "/images/"

//业务处理模块
image_server::TableImage* image_table;//创建一个访问数据库的对象

//计算文件的MD5值
void MD5_CALC(const std::string& body,std::string* md5)
{
	unsigned char buf[16]={0};
	MD5((const unsigned char *)body.c_str(),body.size(),buf);
	for(int i=0;i<16;++i){
		char tmp[3]={0};
		sprintf(tmp,"%02x",buf[i]);
		*md5 += tmp;
	}
}

//将图片数据写入文件中
bool WriteFile(const std::string& filename,const std::string body)
{
	std::ofstream ofs(filename,std::ios::binary);//二进制打开文件
	if(ofs.is_open() == false){
		printf("Open file %s failed!\n",filename.c_str());
		return false;
	}
	ofs.write(body.c_str(),body.size());
	if(ofs.good() == false){
		printf("write file body failed!\n");
		ofs.close();
		return false;
	}
	ofs.close();
	return true;
}

//获取所有图片
void GetAllImage(const httplib::Request& req,httplib::Response& rsp)
{
	printf("Process request to get all picture meta imfomation\n");
	Json::Value images;
	//从数据库获取到所有的元信息
	bool ret=image_table->GetAll(&images);
	if(ret == false){
		printf("failed to get all data from database!\n");
		rsp.status=500;//服务器内部错误
		return;
	}

	//序列化成为字符串，放到响应正文中
	Json::FastWriter writer;
	//rsp.body=writer.write(images);
	//rsp.set_header("Content-Type","application/json");
	//设置响应正文类型
	rsp.set_content(writer.write(images),"application/jsoncpp");
	//设置响应状态码
	rsp.status=200;
	printf("Get all picture meta imformation request processed successfully\n");
	return;
}

//获取单张图片
void GetOneImage(const httplib::Request& req,httplib::Response& rsp)
{
	// path=/image/(\d+)
	//从请求信息种获取图片id
	//req.matches存放匹配字符串： /image/3 
	//req.matches[0]:/image/3   req.matches[1]:3
	int image_id=std::stoi(req.matches[1]);
	//从数据库获取图片元信息
	Json::Value image;
	bool ret=image_table->GetOne(image_id,&image);	
	if(ret == false){
		printf("failed to get one data from database!\n");
		rsp.status=500;//服务器内部错误
		return;
	}
	
	//进行数据序列化，填充到响应正文
	//设置头部字段
	Json::FastWriter writer;
	rsp.set_content(writer.write(image),"application/json");
	//设置响应状态码，不主动设置默认200
	return;
}

//删除图片
void DeleteImage(const httplib::Request& req,httplib::Response& rsp)
{
	//获取要删除的图片id
	int image_id=std::stoi(req.matches[1]);
	//从数据库获取图片元信息,找到图片实际存储路径，删除文件
	Json::Value image;
	bool ret=image_table->GetOne(image_id,&image);	
	if(ret == false){
		printf("failed to get image data when deleting picture!\n");
		rsp.status=500;//服务器内部错误
		return;
	}
	std::string real_path=image["fpath"].asString();
	unlink(real_path.c_str());//删除文件
	
	//从数据库删除图片元信息
	ret=image_table->Delete(image_id);
	if(ret == false){
		printf("failed to delete  image data from database!\n");
		rsp.status=500;//服务器内部错误
		return;
	}
	//设置响应信息，默认200
	return;
}

//上传图片
void AppendImage(const httplib::Request& req,httplib::Response& rsp)
{
	//判断有没有文件
	bool ret=req.has_file("imagefile");
	if(ret == false){
		printf("The name of the upload file is not found!\n");
		rsp.status=400;
		return;
	}

	auto file=req.get_file_value("imagefile");
	//file.filename:文件的文件名  file.content:文件数据
	//计算文件大小，计算文件md5值，计算文件实际存储路径名，计算文件实际存储路径名，计算文件url
	int fsize=file.content.size();
	std::string md5_str;
	MD5_CALC(file.content,&md5_str);
	std::string image_url=IMAGE_PATH+file.filename;// image/test.jpg
	std::string real_path=WWW_ROOT+image_url;// ./www/image/test.jpg

	Json::Value image;
	image["name"]=file.filename;
	image["fsize"]=(Json::Value::Int64)file.content.size();
	image["fpath"]=real_path;
	image["furl"]=image_url;
	image["fmd5"]=md5_str;

	//将图片元数据插入数据库
	ret=image_table->Insert(image);
	if(ret == false){
		printf("Failed to insert image into database!\n");
		rsp.status=500;
		return;
	}
	
	//将图片数据写入文件
	WriteFile(real_path,file.content);

	rsp.status=200;
	rsp.set_redirect("/");//设置一个重定向，重新请求首页
	return;
}

//修改图片内容
void UpdateImage(const httplib::Request& req,httplib::Response& rsp)
{
	//从请求种获取图片id,以及修改后的图片数据
	int image_id=std::stoi(req.matches[1]);
	//要修改的信息在请求的正文中，是一个json字符串
	Json::Value image;
	Json::Reader reader;
	//reader.parse(json字符串,接受解析数据的Json::Value对象)
	bool ret=reader.parse(req.body,image);
	if(ret == false){
		printf("Modify picture is incorrect!\n");
		rsp.status=400;//坏的请求，请求格式有问题
		return;
	}

	//从数据库修改指定图片的元信息
	ret=image_table->Update(image_id,image);
	if(ret == false){
		printf("failed to modify image data from database!\n");
		rsp.status=500;//服务器内部错误
		return;
	}
	return;
}

int main()
{
	//连接数据库
	MYSQL* mysql=image_server::MysqlInit();
	image_table = new image_server::TableImage(mysql);

	httplib::Server srv;

	//设置静态文件资源根目录,当收到文件的请求时httplib自动响应
	//当收到/images/test.png就会自动追上根目录,变成www/images/test.png
	srv.set_base_dir(WWW_ROOT);

	//获取图片元信息请求
	srv.Get("/image",GetAllImage);
	//获取指定图片数据请求----可以不用处理，httplib会自动处理静态文件请求
	//获取指定图片元信息请求
	//正则表达式/image/(\d+) ----匹配imgae/一个或多个数字字符
	//(d+):使用括号表示要捕捉这个数据
	srv.Get(R"(/image/(\d+))",GetOneImage);
	//删除图片请求
	srv.Delete(R"(/image/(\d+))",DeleteImage);
	//上传图片请求
	srv.Post("/image",AppendImage);
	//修改图片信息请求
	srv.Put(R"(/image/(\d+))",UpdateImage);

	srv.listen("0.0.0.0",9000);

	//关闭连接
	image_server::MysqlRelease(mysql);

	return 0;
}
