#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>
#include "../common/Util.hpp"

using namespace std;

//设置全局配置项,对应一些目录位置
string g_input_path="../data/input/";
//预处理之后输出的是一个行文本文件
string outputFile="../data/tmp/raw_input";

//为了进行解析工作，准备一个结构体，表示一个文档
struct DocInfo{
	//文档标题
	string title;
	//文档对应的在线版的文档地址
	string url;
	//去标签的html内容
	string content;
};

//检举出所有的html文件
bool enumFile(const string& input,vector<string>* output){
	//借助boost库的filesystem遍历目录,取个别名
	namespace fs=boost::filesystem;

	fs::path rootpath(input);
	//判断目录是否存在
	if(!fs::exists(rootpath)){
		cout<<"input path not exists!"<<endl;
		return false;
	}

	//遍历目录
	//输入目录中，可能还有子目录
	//通过boost库的方法递归打开所有目录
	fs::recursive_directory_iterator endIter;//默认构造函数得到迭代器的结束位置
	for(fs::recursive_directory_iterator it(rootpath); it != endIter; ++it){
		//此处进行判定
		//1.判断是否为普通文件，如果不是,直接过滤
		//2.判断是否是以.html结尾的文件，不是直接过滤
		if(!fs::is_regular_file(*it)){
			continue;
		}
		if(it->path().extension() != ".html"){
			continue;
		}
		//得到的结果就是 html 的文件的路径
		output->push_back(it->path().string());
	}

	return true;
}

//解析标题
bool parseTitle(const string& html,string* output){
	//根据html中的title标签进行解析
	int begin=html.find("<title>");
	if(begin == string::npos){
		cout<<"parseTitle failed! no begin!"<<endl;
		return false;
	}
	int end=html.find("</title>");
	if(end == string::npos){
		cout<<"parseTitle failed! no end!"<<endl;
		return false;
	}
	//begin向后移动
	begin+=string("<title>").size();
	*output=html.substr(begin,end-begin);
	return true;
}

//解析url
bool parseUrl(const string& path,string* url){
	//path: ../data/input/html/foreach.html
	//url: https://www.boost.org/doc/libs/1_53_0/doc/html/foreach.html
	//g_input_path="../data/input/";
	string head="https://www.boost.org/doc/libs/1_53_0/doc/";
	string tail=path.substr(g_input_path.size());	//html/xxx.html
	*url=head+tail;
	return true;
}

//解析正文
bool parseContent(const string& html,string* content){
	bool isContent=true;
	//遍历html中的每一个字符
	for(auto c:html){
		if(isContent)
		{
			if(c == '<')
			{
				//切换到标签模式
				isContent=false;
			}
			else
			{
				//将换行替换为空格
				if(c == '\n')
					c=' ';
				//将正文写入最终结果
				content->push_back(c);
			}
		}
		else{
			//当前在操作标签
			//如果当前操作的是>,切换到正文状态
			if(c == '>')
			{
				isContent=true;
			}
			//忽略标签内的其他内容
		}
	}
	return true;
}

//解析文档
bool parseFile(const string& input,DocInfo* docInfo){
	//0.读取文件内容
	string html;
	//这是一个功能比较底层的函数，放到common目录中
	bool ret=common::Util::read(input,&html);
	if(!ret){
		cout<<"read file failed!"<<endl;
		return false;
	}

	//1.解析文档标题(文件名)
	ret=parseTitle(html,&docInfo->title);
	if(!ret){
		cout<<"parse title false!"<<endl;
		return false;
	}

	//2.解析文档的url(文件路径)
	ret=parseUrl(input,&docInfo->url);
	if(!ret){
		cout<<"parse url false!"<<endl;
		return false;
	}

	//3.解析文档的正文(去除html标签)
	ret=parseContent(html,&docInfo->content);
	if(!ret){
		cout<<"parse content false!"<<endl;
		return false;
	}
	return true;
}

//将文档信息写入文件
void writeOutput(const DocInfo& docInfo,ofstream& outputFile){
	//用不可见字符作为切割标记，防止粘包
	outputFile<<docInfo.title<<"\3"<<docInfo.url<<"\3"<<docInfo.content<<endl;
}

int main(){
	//1.枚举出input目录中的所有文件路径，以便后续进行打开和操作
	vector<string> fileList;
	if(!enumFile(g_input_path,&fileList)){
		cout<<"enumFail failed!"<<endl;
		return -1;
	}

	ofstream output_file(outputFile);
	if(!output_file.is_open()){
		cout<<"open output file failed!"<<endl;
		return -1;
	}
	for(const auto& f:fileList){
		//2.依次解析每个文档结构，进行去标签操作
		//展示遍历到的html文件的结果
		cout<<"parse file: "<<f<<endl;
		//针对文件进行解析
		DocInfo docInfo;
		if(!(parseFile(f,&docInfo))){
				cout<<"parseFile failed!"<<endl;
				continue;
		}
		//3.将每个文件处理后的结果存入行文本文件
		writeOutput(docInfo,output_file);
	}

	//关闭文件，防止内存泄露
	output_file.close();
	return 0;
}
