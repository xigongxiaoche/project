#include"searcher.h"
//索引模块的简单测试

int main(){
	using namespace std;

	searcher::Index index;
	bool ret=index.build("../data/tmp/raw_input");
	if(!ret){
		cout<<"build failed "<<endl;
		return -1;
	}

	//查正排和倒排
	auto* invertedList = index.getInverted("filesystem");
	for(const auto& weight:*invertedList){
		cout<<"倒排：-----------------------------------------------------------"<<endl;
		cout<<weight.docId<<";"<<weight.weight<<";"<<weight.word<<endl;
		cout<<"-----------------------------------------------------------"<<endl;
		//根据docId查正排
		const searcher::DocInfo* docInfo=index.getDocInfo(weight.docId);
		cout<<"docId:"<<docInfo->docId<<";title:"<<docInfo->title<<";url:"
			<<docInfo->url<<endl;
		cout<<docInfo->content<<endl;
	}
	return 0;
}
