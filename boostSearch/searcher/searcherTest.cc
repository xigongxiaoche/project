#include "searcher.h"
#include<iostream>
#include<string>

int main(){
	using namespace std;
	searcher::Searcher searcher;
	bool ret=searcher.init("../data/tmp/raw_input");
	if(ret == false){
		cout<<"init failed!"<<endl;
		return -1;
	}

	while(true){
		string query;
		string res;
		cout<<"请输入查询关键词:"<<flush;
		cin>>query;
		if(!cin.good()){
			cout<<"goodBye"<<endl;
			break;
		}
		ret=searcher.search(query,&res);
		if(ret == false){
			cout<<"query failed"<<endl;
			return -1;
		}
		cout<<"查询结果如下:"<<endl;
		cout<<res<<endl;
	}
	return 0;
}
