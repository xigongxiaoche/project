#include <iostream>
#include <fstream>
#include <boost/algorithm/string.hpp>
#include "searcher.h"
#include "../common/Util.hpp"
#include <algorithm>
#include <jsoncpp/json/json.h>

using namespace std;

namespace searcher{

////////////////////////////////////////////
// Index模块相关代码
////////////////////////////////////////////

//分词词典路径
const char* const DICT_PATH = "../jieba_dict/jieba.dict.utf8";
const char* const HMM_PATH = "../jieba_dict/hmm_model.utf8";
const char* const USER_DICT_PATH = "../jieba_dict/user.dict.utf8";
const char* const IDF_PATH = "../jieba_dict/idf.utf8";
const char* const STOP_WORD_PATH = "../jieba_dict/stop_words.utf8";


//Index构造函数初始化jieba对象
Index::Index()
	:jieba(DICT_PATH,
	HMM_PATH,
	USER_DICT_PATH,
	IDF_PATH,
	STOP_WORD_PATH){
}

//分词
void Index::cutWord(const string& word,vector<string>* tokens){
	jieba.CutForSearch(word,*tokens);
}

//根据文档id获取正排索引内容
const DocInfo* Index::getDocInfo(int64_t docId){
	if(docId >= (int64_t)forwardIndex.size()){
		return nullptr;
	}
	return &forwardIndex[docId];
}

//根据词，获取倒排拉链
const InvertedList* Index::getInverted(const string& key){
	auto it = invertedIndex.find(key);
	//没找到倒排拉链
	if(it == invertedIndex.end()){
		return nullptr;
	}
	return &it->second;
}

//创建正排索引
DocInfo* Index::buildForward(const string& line){
	//1.按照\3进行切割
	vector<string> tokens;
	common::Util::split(line,"\3",&tokens);
	if(tokens.size() != 3){
		return nullptr;
	}
	
	//2.创建DocInfo对象，并将分割的内容进行分割
	DocInfo docInfo;
	docInfo.docId=forwardIndex.size();
	docInfo.title=tokens[0];
	docInfo.url=tokens[1];
	docInfo.content=tokens[2];
	//forwardIndex.push_back(docInfo);
	//将docInfo直接搬运----C++11
	forwardIndex.push_back(std::move(docInfo));
	//3.返回得到的DocInfo对象的指针，供倒排索引的构造来使用
	//不能返回 &DocInfo,因为退出作用域之后就会销毁DocInfo,再去解引用就会使用野指针
	return &forwardIndex.back();
}


//构建倒排索引
void Index::buildInverted(const DocInfo& docInfo){
	//创建用于统计词频的结构体
	struct WordCnt{
		int titleCnt;  //标题中出现的次数
		int contentCnt;//正文中出现的次数
		WordCnt() : titleCnt(0), contentCnt(0){}
	};
	//使用hash表进行词频的统计
	unordered_map<string,WordCnt> wordCntMap;

	//1.针对文档标题进行分词
	vector<string> titleTokens;
	cutWord(docInfo.title,&titleTokens);
	//2.根据分词结果，统计每个词在标题中出现的次数
	for(string word:titleTokens){
		//不区分大小写，全部转换为小写
		boost::to_lower(word);
		++wordCntMap[word].titleCnt;
	}
	//3.针对文档正文进行分词
	vector<string> contentTokens;
	cutWord(docInfo.content,&contentTokens);
	//4.根据分词结果，统计每个词在正文中的出现次数
	for(string word:contentTokens){
		//不区分大小写，全部转换为小写
		boost::to_lower(word);
		++wordCntMap[word].contentCnt;
	}
	//5.遍历统计结果，构建倒排索引
	//(key是词，value是权重)
	//auto得到的类型是一个pair
	for(auto wordPair : wordCntMap){
		Weight weight;
		weight.docId=docInfo.docId;
		//权重的算法:标题中出现的次数*10+正文中出现的次数
		weight.weight=wordPair.second.titleCnt * 10 + wordPair.second.contentCnt;
		//将这个词在weight对象中也存储一份，以备后用
		weight.word=wordPair.first;
		//更新倒排索引
		//根据当前词，在倒排索引中查找对应的倒排拉链
		//存在的话返回对应的倒排拉链的引用
		//不存在创建一个元素，并返回key为当前词的映射值的引用
		//把权重对象插入到倒排拉链尾部
		InvertedList& invertedList = invertedIndex[wordPair.first];
		invertedList.push_back(weight);
	}
}

//构建索引
//inputPath指向预处理模块处理之后的行文本文件 raw_input
//这个输入文件中每行对应一个文档
//每行分为 title url content 三部分，中间用\3隔开
bool Index::build(const string& inputPath){
	cout<<"build index start!"<<endl;
	//1，按行读取文件内容
	ifstream file(inputPath.c_str());
	if(!file.is_open()){
		cout<<"build inputPath failed! inputPath: "<<inputPath<<endl;
		return false;
	}

	string line;
	while(getline(file,line)){
		//2.根据当前行，构造DocInfo对象并插入正排索引中
		DocInfo* docInfo=buildForward(line);
		if(docInfo == nullptr){
			continue;
		}

		//3.解析DocInfo中的内容,把结构插入到倒排索引中
		buildInverted(*docInfo);
		//每次构建100个索引打印一下，显示进度条
		if(docInfo->docId % 100 == 0){
			cout<<"index build docId:"<<docInfo->docId<<endl;
		}
	}

	file.close();
	cout<<"build index finish!"<<endl; 
	return true;
}

//////////////////////////////////////////////
// 搜索相关代码
//////////////////////////////////////////////

	 //获取摘要
	 string Searcher::generateDesc(const string& content,const string& word){
		//1.找到该词在正文中出现的位置
		int pos=content.find(word);
		if(pos == (int64_t)string::npos){
			//正文中没找到
			//这种情况一般不会出现，词在标题中能够出现，在正文中没有出现
			return "";
		}
		//2.以该位置为中心，向前找60个字符,向后找100个字符，连成一个段落
		//要注意边界条件
		int64_t begin=pos<60 ? 0 : pos-60;
		if(begin + 160 >= (int64_t)content.size()){
			//词出现的位置靠近文件末尾
			return content.substr(begin);
		}else{
			string desc=content.substr(begin,160);
			desc[desc.size()-1]='.';
			desc[desc.size()-2]='.';
			desc[desc.size()-3]='.';
			return desc;
		}
	 }

	//通过对 inputPath 进行构造索引， inputPath 就指向 data/tmp/raw_input
	bool Searcher::init(const std::string& inputPath){
		return index->build(inputPath);
	}

	//处理搜索
	bool Searcher::search(const std::string& query, std::string* output){
		//1.分词:对查询词进行分词
		vector<string> tokens;
		index->cutWord(query,&tokens);
		//2.触发:根据分词结果，查倒排，找到相关的文档id
		vector<Weight> allTokens;//存放查询的所有的
		for(string word : tokens){
			//查倒排之前忽略大小写
			boost::to_lower(word);
			const auto* invertedList = index->getInverted(word);
			if(invertedList == nullptr){
				//该词没找到
				continue;
			}
			//找到查询结果，将查询结果合并到一个大的数组中
			//分词结果可能是多个，将每个单词的倒排拉链合并为一个数组
			//然后对数组进行排序
			allTokens.insert(allTokens.end(),invertedList->begin(),invertedList->end());
		}	
		//3.排序:根据该词在该文档中的次数，对结果进行排序
		//按照权重降序排列
		std::sort(allTokens.begin(),allTokens.end(),
			[](const Weight& w1, const Weight& w2){
				return w1.weight > w2.weight;
			});
		//4.构造结果:根据最终的结果查正排，构造json格式的数据
		//Json::Value这个类可以当作vector,也可以当作map使用
		Json::Value results;
		for(const auto& weight : allTokens){
			//根据weight中的docId,查正排
			//将查询结果的相关内容，构造成json格式的字符串
			const auto* docInfo = index->getDocInfo(weight.docId);
			Json::Value result;
			result["title"]=docInfo->title;
			result["url"]=docInfo->url;
			result["desc"]=generateDesc(docInfo->content,weight.word);//获取正文摘要
			results.append(result);
		}

		//将Json::Value对象序列化转为字符串，写入到output这个字符串中
		Json::FastWriter writer;
		*output=writer.write(results);
		return true;
	}


} //end searcher
