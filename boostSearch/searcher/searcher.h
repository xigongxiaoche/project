#pragma once

#include <vector>
#include <string>
#include <unordered_map>
#include <stdint.h>
#include <cppjieba/Jieba.hpp> 

using std::string;
using std::vector;
using std::unordered_map;

namespace searcher{

////////////////////////////////
//索引相关代码
////////////////////////////////

//文档信息
struct DocInfo{
	int64_t docId;//文档id
	string title;//标题
	string url;//url
	string content;//内容
};

//倒排索引的value
struct Weight{
	int64_t docId;//文档id
	int weight;//权重
	string word;//备份
};

//倒排拉链
typedef vector<Weight> InvertedList;

//索引类
//两个主属性:正排索引，倒排索引
//三个主方法:查正排，查倒排，构建索引
class Index{
private:
	vector<DocInfo> forwardIndex;//正排索引
	unordered_map<string, InvertedList> invertedIndex;//倒排索引:内容+Weight
public:
	//根据文档id获取正排索引内容
	const DocInfo* getDocInfo(int64_t docId);
	//根据词，获取倒排拉链
	const InvertedList* getInverted(const string& key);
	//构建索引
	//inputPath指向预处理模块处理之后的行文本文件 raw_input
	bool build(const string& inputPath);
	//构造函数完成jieba分词对象的初始化
	Index();
	
	//分词
	void cutWord(const string& word,vector<string>* tokens);
private:
	//创建正排索引
	DocInfo* buildForward(const string& line);
	//构建倒排索引
	void buildInverted(const DocInfo& docInfo);
	cppjieba::Jieba jieba;//分词对象
};


///////////////////////////////////////////////////
//  搜索相关代码
///////////////////////////////////////////////////

class Searcher{
private:
	Index* index;//索引对象

public:
	Searcher() :index(new Index()) {} 
	//通过对 inputPath 进行构造索引， inputPath 就指向 data/tmp/raw_input
	bool init(const std::string& inputPath);
	//处理搜索
	bool search(const std::string& query, std::string* output);

private:
	//获取摘要
	static string generateDesc(const string& content,const string& word);
};

} //end searcher
