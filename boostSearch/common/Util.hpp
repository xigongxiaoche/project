#pragma once

#include<iostream>
#include<string>
#include<fstream>
#include<vector>
#include<boost/algorithm/string.hpp>

namespace common{

class Util{
public:
	static bool read(const std::string input,std::string* output){
		//根据input字符串的内容打开文件，读取文件内容并放到output中
		//以二进制方式打开,以字节为单位进行读取
		std::ifstream file(input.c_str(),std::ios::binary); 
		if(!file.is_open()){
			std::cout<<"open file failed!"<<std::endl;
			return false;
		}
		//根据文件长度,调整空间大小，将数据读取进去
		file.seekg(0,file.end);//跳转到文件末尾
		int length=file.tellg();//获取文件长度
		file.seekg(0,file.beg);//跳转到文件开头
		//根据文件大小，调整空间
		output->resize(length);
		file.read(const_cast<char*>(output->data()),length);
		file.close();
		return true;
	}

	//调用boost库的split来实现
	static void split(const std::string& input,const std::string& delimiter,
		std::vector<std::string>* output){
		//token_compress_off:不压缩分隔符
		boost::split(*output,input,boost::is_any_of(delimiter),boost::token_compress_off);
	}
};

} //end common
