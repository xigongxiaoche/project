#include <iostream>
#include <string>
#include <vector>
#include "httplib.h"
#include "../searcher/searcher.h"

int main(){
	using namespace std;
	using namespace httplib;

	//1.初始化搜索模块
	searcher::Searcher searcher;
	bool ret=searcher.init("../data/tmp/raw_input");
	if(ret == false){
		cout<<"init failed!"<<endl;
		return -1;
	}

	//2.初始化服务器模块
	//   约定客户端通过query这样的参数来传递查询词
	//   浏览器搜索使用的url如下：
	//   http://120.27.238.85:9000/searcher?query=filesystem
	Server srv;
	srv.Get("/searcher",[&searcher](const Request& req,Response& resp){
		//1.  获取到query参数
		bool ret=req.has_param("query");
		if(!ret){
			resp.set_content("query param miss!","text/plain");
			return;
		}
		string query = req.get_param_value("query");
		//2.  使用searcher对象进行搜索
		string result;
		searcher.search(query,&result);
		resp.set_content(result,"application/json");
	});
	
	srv.set_base_dir("./wwwroot");//设置静态根目录
	srv.listen("0.0.0.0",9000);//设置访问的IP和端口号-----本机任意网卡:9000
	return 0;
}
