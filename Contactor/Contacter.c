#include "Contacter.h"

/////////////////////////////////////////////////////////////
//	内部接口：供内部功能调用

//通讯录容量达到上限进行增容
static void CheckCapacity(MailList* mlst)
{
	if (mlst == NULL || mlst->count < mlst->capactiy)
	{
		return;
	}

	//开辟新空间，拷贝数据
	int newC = 2 * mlst->capactiy;
	Contactor* tmp = (Contactor*)calloc(newC, sizeof(Contactor));
	memcpy(tmp, mlst->list, sizeof(Contactor)* mlst->capactiy);
	//释放旧空间
	free(mlst->list);
	
	//更新
	mlst->list = tmp;
	mlst->capactiy = newC;
}

//从文件中读取数据存储到通讯录中
static void ReadDataFromFile(MailList* mlst)
{
	//遇见了一个问题，if后面的FILE编译器无法识别
	//给if后面加上括号就没问题了
	if (mlst == NULL)
	{
		return;
	}

	//打开文件
	FILE*  fp_read = fopen("data.txt", "rb");
	if (fp_read == NULL)
	{
		printf("打开文件失败!\n");
		return;
	}
	int curIdx = 0;
	Contactor personInfo;
	//读取到数据后将数据存储到通讯录中
	while (fread(&personInfo, sizeof(Contactor), 1, fp_read) > 0)
	{
        //将数据写入通讯录之前检查通讯录容量
        CheckCapacity(mlst);
        
		mlst->list[curIdx++] = personInfo;
		//更新通讯录元素个数
		mlst->count++;
	}

	//关闭文件
	fclose(fp_read);
}

//将数据从通讯录写入文件
//mode是写入的模式
static void WriteDataFromContactor(MailList* mlst, const char* mode)
{
	if (mlst == NULL)
	{
		return;
	}

	//打开文件
	FILE* fp_write = fopen("data.txt", mode);
	if (fp_write == NULL)
	{
		printf("打开文件失败!\n");
		return;
	}

	//将通讯录中的数据写入文件
	for (int idx = 0; idx < mlst->count; ++idx)
	{
		fwrite(&(mlst->list[idx]), sizeof(Contactor), 1, fp_write);
	}

	//关闭文件
	fclose(fp_write);
}

//判断联系人是否已经存在
//返回值-1表示不存在
static int IsExist(MailList* mlst, Contactor* info)
{
	if (mlst == NULL || info == NULL)
		return -1;

	for (int idx = 0; idx < mlst->count; ++idx)
	{
		if (strcmp(mlst->list[idx].Name, info->Name) == 0)
		{
			return idx;
		}
	}
	return -1;
}

//	end of Inside Interface
/////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////
//	外部接口：提供给用户使用

//初始化通讯录
void InitMailList(MailList* mlst)
{
	if (mlst == NULL)
	{
		return;
	}

	mlst->count = 0;
	mlst->capactiy = ORIGINCAPACITY;
	mlst->list = (Contactor*)calloc(mlst->capactiy, sizeof(Contactor));

	//文件中有数据就将文件中的数据读入通讯录
	ReadDataFromFile(mlst);
}

//销毁通讯录
void DestroyMailList(MailList* mlst)
{
	if (mlst == NULL)
	{
		return;
	}

	mlst->count = 0;
	mlst->capactiy = 0;
	free(mlst->list);
}

//菜单
void Menu()
{
	printf("*******************************************************************\n");
	printf("*********************		通讯录     ************************\n");
	printf("*********************  0: 退出通讯录   ****************************\n");
	printf("*********************  1: 添加联系人   ****************************\n");
	printf("*********************  2: 删除联系人   ****************************\n");
	printf("*********************  3: 查找联系人   ****************************\n");
	printf("*********************  4: 修改联系人    ***************************\n");
	printf("*********************  5: 按照姓名对所有联系人排序*****************\n");
	printf("*********************  6: 清空通讯录    ***************************\n");
	printf("*********************  7: 显示所有联系人    ***********************\n");
	printf("*******************************************************************\n");
}
//添加联系人
void AddContacter(MailList* mlst)
{
	if (mlst == NULL)
	{
		return;
	}

	//1.将个人信息保存到一个Contactor变量中
	Contactor personInfo;
	printf("请输入姓名: ");
	scanf("%s", &(personInfo.Name));
	printf("请输入性别(0表示女性,1表示男性): ");
	scanf("%d", &(personInfo.sex));
	printf("请输入年龄: ");
	scanf("%d", &(personInfo.age));
	printf("请输入手机号: ");
	scanf("%s", &(personInfo.PhoneNumber));
	printf("请输入通讯地址: ");
	scanf("%s", &(personInfo.address));
	printf("请输入职业: ");
	scanf("%s", &(personInfo.Profession));

	//2.将新建联系人存入通讯录
	//存入联系人之前要检查联系人是否存在，并检查容量
	if (IsExist(mlst, &personInfo) != -1)
	{
		printf("联系人 %s已经在通讯录中!\n", personInfo.Name);
		return;
	}
	CheckCapacity(mlst);
	mlst->list[mlst->count] = personInfo;
	mlst->count++;

	//3.将通讯录信息追加到文件尾部
	WriteDataFromContactor(mlst, "ab+");
}

//根据姓名查找联系人在通讯录中的下标
//找不到返回-1
int FindContacter(MailList* mlst)
{
	if (mlst == NULL)
		return -1;

	int isFind = 0;			//是否找到联系人
	char Name[MAXNAME] = { 0 };
	printf("请输入要查找的联系人姓名: ");
	scanf("%s", Name);
	for (int idx = 0; idx < mlst->count; ++idx)
	{
		if (strcmp(Name, mlst->list[idx].Name) == 0)
		{
			//找到联系人就打印出联系人信息
			Contactor personInfo = mlst->list[idx];
			printf("姓名: %s\t", personInfo.Name);
			printf("性别: ");
			if (personInfo.sex == 0)
			{
				printf("女\n");
			}
			else
			{
				printf("男\n");
			}
			printf("职业: %s\t年龄: %d\n", personInfo.Profession, personInfo.age);
			printf("手机号: %s\n", personInfo.PhoneNumber);
			printf("通讯地址: %s\n", personInfo.address);

			isFind = 1;		//更改标记位
			return idx;
		}
	}
	
	if (isFind == 0)
	{
		printf("没有找到联系人 %s\n", Name);
	}
	return -1;
}

//删除联系人
void DeleteContacter(MailList* mlst)
{
	if (mlst == NULL)
	{
		return;
	}

	//查找到联系人在通讯录中所在下标
	int goalIdx = FindContacter(mlst);
	if (goalIdx == -1)
	{
		printf("要删除的联系人不存在!\n");
		return;
	}

	//移动通讯录中的数据
	for (int idx = goalIdx; idx < mlst->count - 1; ++idx)
	{
		mlst->list[idx] = mlst->list[idx + 1];
	}
	//将最后一位置为0，防止对新插入的数据造成影响
	memset(&(mlst->list[mlst->count - 1]), 0, sizeof(Contactor));
	//更新元素个数
	mlst->count--;
	//将修改后的通讯录数据覆盖写入文件
	WriteDataFromContactor(mlst, "wb");
}

//修改联系人信息
void ModifyContacter(MailList* mlst)
{
	if (mlst == NULL)
	{
		return;
	}

	//查找到联系人在通讯录中所在下标
	int goalIdx = FindContacter(mlst);
	if (goalIdx == -1)
	{
		printf("要查找的联系人不在通讯录中!\n");
		return;
	}

	//重新编辑联系人信息
	Contactor* personInfo = &(mlst->list[goalIdx]);
	printf("请输入姓名: ");
	scanf("%s", &(personInfo->Name));
	printf("请输入性别(0表示女性,1表示男性): ");
	scanf("%d", &(personInfo->sex));
	printf("请输入年龄: ");
	scanf("%d", &(personInfo->age));
	printf("请输入手机号: ");
	scanf("%s", &(personInfo->PhoneNumber));
	printf("请输入通讯地址: ");
	scanf("%s", &(personInfo->address));
	printf("请输入职业: ");
	scanf("%s", &(personInfo->Profession));

	//将修改后的通讯录信息覆盖写入文件中
	WriteDataFromContactor(mlst, "wb");
}

//显示所有联系人
void ShowAllContacter(MailList* mlst)
{
	if (mlst == NULL)
	{
		return;
	}

	if (mlst->count == 0)
	{
		printf("通讯录中没有联系人!\n");
		return;
	}

	printf("总共有 %d 位联系人, 所有联系人信息如下:\n", mlst->count);
	for (int idx = 0; idx < mlst->count; ++idx)
	{
		Contactor personInfo = mlst->list[idx];
		printf("姓名: %s\t", personInfo.Name);
		printf("性别: ");
		if (personInfo.sex == 0)
		{
			printf("女\n");
		}
		else
		{
			printf("男\n");
		}
		printf("职业: %s\t年龄: %d\n", personInfo.Profession, personInfo.age);
		printf("手机号: %s\n", personInfo.PhoneNumber);
		printf("通讯地址: %s\n", personInfo.address);
		printf("****************************************************\n");
	}
}

//清空通讯录
void ClearMailList(MailList* mlst)
{
	if (mlst == NULL || mlst->count == 0)
		return;

	//将通讯录中的数据置为最初的状态
	mlst->count = 0;
	mlst->capactiy = ORIGINCAPACITY;
	memset(mlst->list, 0, sizeof(Contactor)* mlst->capactiy);

	//删除文件
	remove("data.txt");
	printf("清空通讯录成功!\n");
}

//按照姓名首字母从小到大排序
int CompareByName(const void* a, const void* b)
{
	Contactor* str = (Contactor*)a;
	Contactor* str2 = (Contactor*)b;
	return strcmp(str->Name, str2->Name) > 0;
}

//按照姓名对所有联系人排序
void SortContacterByName(MailList* mlst)
{
	if (mlst == NULL)
	{
		return;
	}

	//按照姓名首字母从小到大排序
	qsort(mlst->list, mlst->count, sizeof(Contactor), CompareByName);
	printf("按照姓名排序成功!\n");
}

//	end of OutSide Interface
/////////////////////////////////////////////////////////////