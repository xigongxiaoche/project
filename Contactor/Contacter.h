#pragma once

#include <stdio.h>
#include <stdlib.h>		//malloc、system
#include <string.h>		//strcmp

//通过文件存储通讯录信息

#define MAXNAME 100			//姓名最大长度
#define MAXPHONENUMBER 12	//手机号最大长度
#define MAXADDRESS 128		//通讯地址最大长度
#define MAXPROFESSION 32	//职业名最大长度 
#define ORIGINCAPACITY 50	//通讯录最初容量

//////////////////////////////////////////////////////////////
//	通讯录数据结构模块

//性别
enum Sex
{
	Woman,
	Man
};

//联系人信息
//让占用的字节数大的排列到前面，这样内存对齐时占用的内存小
typedef struct Contactor
{
	char address[MAXADDRESS];			//通讯地址
	char Name[MAXNAME];					//姓名
	char Profession[MAXPROFESSION];		//职业
	char PhoneNumber[MAXPHONENUMBER];	//手机号
	enum Sex sex;						//性别
	int age;							//年龄
}Contactor;

//通讯录
typedef struct MailList
{
	Contactor* list;	//联系人信息
	int count;			//当前数据条数
	int capactiy;		//通讯录容量
}MailList;
//	end of DataStructure
//////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////
/*
	内部接口只允许在本源文件中使用，不允许外部调用，用static进行修饰
	内部接口：
		从文件中读取数据存储到通讯录中
		将数据从通讯录写入文件
		通讯录容量达到上限进行增容
*/

//通讯录容量达到上限进行增容
static void CheckCapacity(MailList* mlst);

//从文件中读取数据存储到通讯录中
static void ReadDataFromFile(MailList* mlst);

//将数据从通讯录写入文件
//mode是写入的模式
static void WriteDataFromContactor(MailList* mlst, const char* mode);

//判断联系人是否已经存在
//返回值-1表示不存在
static int IsExist(MailList* mlst, Contactor* info);

//	end of Inside Interface
//////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////
/*
通讯录对外提供的功能：
	添加联系人
	删除联系人
	根据姓名查找联系人在通讯录中的下标
	修改联系人信息
	显示所有联系人
	清空通讯录
	按照姓名对所有联系人排序
	退出通讯录
	初始化通讯录
	销毁通讯录
	按照姓名首字母从小到大排序
*/

//初始化通讯录
void InitMailList(MailList* mlst);

//销毁通讯录
void DestroyMailList(MailList* mlst);

//菜单
void Menu();

//添加联系人
void AddContacter(MailList* mlst);

//删除联系人
void DeleteContacter(MailList* mlst);

//根据姓名查找联系人在通讯录中的下标
int FindContacter(MailList* mlst);

//修改联系人信息
void ModifyContacter(MailList* mlst);

//显示所有联系人
void ShowAllContacter(MailList* mlst);

//清空通讯录
void ClearMailList(MailList* mlst);

//按照姓名首字母从小到大排序
int CompareByName(const void* a, const void* b);

//按照姓名对所有联系人排序
void SortContacterByName(MailList* mlst);

//	end of Outside Interface
//////////////////////////////////////////////////////////////