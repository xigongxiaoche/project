#include "Contacter.h"

//通讯录程序
void Controler()
{
	MailList mlst;
	InitMailList(&mlst);
	while (1)
	{
		Menu();
		int choice;
		printf("请输入你的按键: ");
		scanf("%d", &choice);
		if (choice == 0)
		{
			printf("欢迎再次使用通讯录\n");
			break;
		}
		else if (choice == 1)
		{
			AddContacter(&mlst);
			system("pause");
			system("cls");
		}
		else if (choice == 2)
		{
			DeleteContacter(&mlst);
			system("pause");
			system("cls");
		}
		else if (choice == 3)
		{
			FindContacter(&mlst);
			system("pause");
			system("cls");
		}
		else if (choice == 4)
		{
			ModifyContacter(&mlst);
			system("pause");
			system("cls");
		}
		else if (choice == 5)
		{
			SortContacterByName(&mlst);
			system("pause");
			system("cls");
		}
		else if (choice == 6)
		{
			ClearMailList(&mlst);
			system("pause");
			system("cls");
		}
		else if (choice == 7)
		{
			ShowAllContacter(&mlst);
			system("pause");
			system("cls");
		}
		else 
		{
			printf("按键不存在，请重新输入!\n");
			system("pause");
			system("cls");
			continue;
		}
	}
	DestroyMailList(&mlst);
}

int main()
{
	Controler();

	return 0;
}