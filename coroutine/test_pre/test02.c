#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <ucontext.h>

void fun()
{
	printf("this is fun()\n");
}

int main()
{
	int count=0;
	char* stack=(char*)malloc(sizeof(char)*8192);
	ucontext_t mainContext,funContext;

	//保存上下文信息
	getcontext(&mainContext);
	getcontext(&funContext);
	
	printf("%d\n",count++);
	sleep(1);
	
	//设置上下文信息
	funContext.uc_stack.ss_sp=stack;
	funContext.uc_stack.ss_size=8192;
	funContext.uc_stack.ss_flags=0;
	funContext.uc_link=&mainContext;
	
	makecontext(&funContext, fun, 0);

	
	//恢复上下文信息
	setcontext(&funContext);

	printf("main exit\n");
	return 0;
}

