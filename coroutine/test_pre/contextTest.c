#include <stdio.h>
#include <ucontext.h>
#include <unistd.h>

int main()
{
	int i=0;
	ucontext_t context;

	getcontext(&context);
	printf("%d\n",i++);
	sleep(1);
	setcontext(&context);

	return 0;
}
