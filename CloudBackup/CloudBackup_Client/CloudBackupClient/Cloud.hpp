#pragma once

//some common interfaces

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <unordered_map>
#include <experimental/filesystem>
#include <stdint.h>
#ifdef _WIN32
#include <Windows.h>
#else 
#include <unistd.h>
#endif
#include "httplib.h"
#include <utility>

namespace CloudBackup
{
	namespace fs = std::experimental::filesystem;	//c++17 filesystem

	//directory scan
	class ScanDir {
	private:
		std::string _path;	//directory route
	public:
		ScanDir(const std::string& path)
			:_path(path)
		{
			//Judge whether the directory exists. 
			//If it does not exist, create a new directory
			if (!fs::exists(_path))
			{
				//Directory hierarchies can also be created
				fs::create_directories(fs::path(_path));
			}
			//Ensure that the directory ends with /
			if (_path.back() != '/')
			{
				_path += '/';
			}
		}

		//Directory traversal lists the pathnames of all file names.
		//The depth of directory scanning is 1
		bool Scan(std::vector<std::string>* filePaths)
		{
			for (auto& file : fs::directory_iterator(_path))
			{
				std::string name;
				name = file.path().filename().string();
				//Skip directory
				if (fs::is_directory(file.path()))
				{
					continue;
				}
				//Insert file path into result set
				filePaths->push_back(_path + name);
			}
			return true;
		}
	}; //end of ScanDir



	//Tool interface
	class Util {
	public:
		//read content of file
		static bool FileRead(const std::string& file, std::string* body)
		{

			std::ifstream infile;
			infile.open(file, std::ios::binary);
			if (!infile.is_open())
			{
				std::cout << "open file failed int read file!" << std::endl;
				return false;
			}
			//get size of file 
			uint64_t len = fs::file_size(file);
			//Capacity must be increased, 
			//otherwise it is invalid to insert data into an empty container
			body->resize(len);
			//read data of file
			infile.read(&((*body)[0]), len);
			if (!infile.good())
			{
				std::cout << "read data failed int read file!" << std::endl;
				return false;
			}
			//close file
			infile.close();
			return true;
		}

		//write data into file
		static bool FileWrite(const std::string& file, std::string& body)
		{
			std::ofstream ofs;
			ofs.open(file, std::ios::binary);
			if (!ofs.is_open())
			{
				std::cout << "open file failed int write file!" << std::endl;
				return false;
			}
			ofs.write(&body[0], body.size());
			if (!ofs.good())
			{
				std::cout << "write data failed int read file!" << std::endl;
				return false;
			}
			ofs.close();
			return true;
		}

		//cut words
		static int Split(const std::string& content, const std::string& spacer,
			std::vector<std::string>* SplitRes)
		{
			int cnt = 0;
			size_t start = 0, pos = 0;
			while (start < content.size())
			{
				pos = content.find(spacer, start);
				if (pos == std::string::npos)
				{
					break;
				}

				std::string tmp = content.substr(start, pos - start);
				SplitRes->push_back(tmp);
				start = pos + spacer.size();
				++cnt;
			}
			if (start < content.size())
			{
				std::string tmp = content.substr(start);
				SplitRes->push_back(tmp);
				++cnt;
			}
			return cnt;
		}
	};	//end of Util



	//manage data
	class DataManager
	{
	private:
		std::string _path;	//path of config file
		std::unordered_map<std::string, std::string> _map;	//key infomation of file
	public:
		DataManager(const std::string& path)
			:_path(path)
		{}

		//Reads data from a file into a hash table in memory
		bool Read()
		{
			std::string content;
			bool ret = Util::FileRead(_path, &content);
			if (!ret)
			{
				std::cout << "read data failed from file to unordered_map!" << std::endl;
				return false;
			}

			std::vector<std::string> content_v;
			Util::Split(content, "\3", &content_v);
			for (const auto& fileInfo : content_v)
			{
				std::vector<std::string> file_v;
				Util::Split(fileInfo, "=", &file_v);
				_map[file_v[0]] = file_v[1];
			}

			return true;
		}

		//Writes data from a hash table in memory to a file
		bool Write()
		{
			std::string content;
			for (const auto& kv : _map)
			{
				content += kv.first + "=" + kv.second + "\3";
			}
			if (!Util::FileWrite(_path, content))
			{
				std::cout << "write data failed from memory to file!" << std::endl;
				return false;
			}
			return true;
		}

		//Add data to a hash table in memory
		bool Add(const std::string& key, const std::string& val)
		{
			if (Exists(key))
			{
				std::cout << "Add data failed into hashTale of memory!" << std::endl;
				return false;
			}
			_map[key] = val;
			return true;
		}

		//Delete data from hash table in memory
		bool Delete(const std::string& key)
		{
			if (!Exists(key))
			{
				std::cout << "key not exists in HashTable!" << std::endl;
				return false;
			}
			_map.erase(key);
			return true;
		}

		//Modify data of hash table in memory
		bool Modify(const std::string& key, const std::string& val)
		{
			_map[key] = val;
			return true;
		}

		//Determine whether the hash table in memory exists
		bool Exists(const std::string& key)
		{
			if (_map.count(key))
			{
				return true;
			}
			return false;
		}

		//Get the value corresponding to the hash table key in memory
		bool Get(const std::string& key, std::string* val)
		{
			if (!Exists(key))
			{
				return false;
			}
			*val = _map[key];
			return true;
		}
	};	//end of DataManager


#define Monitoringdirectory "./MonitorDir"		//Monitoring directory
#define ConfigFile "./Config.dat"				//config file
	//client
	class Client
	{
	private:
		ScanDir _scd;				//Directory scan object
		DataManager _dma;			//Data management object
		httplib::Client* _cli;		//httplib client

	public:
		Client(const std::string& host, int port)
			:_scd(Monitoringdirectory)
			, _dma(ConfigFile)
			, _cli(new httplib::Client(host, port))
		{}

		//Get the file information that needs to be backed up
		//Backupfileinfo: output type parameter
		//The first of each member is the file path,
		//and the second is the unique identifier of the file	
		bool Scan(std::vector<std::pair<std::string, std::string>>* BackUpFileInfo)
		{
			//Get all file paths in the directory
			std::vector<std::string> filePaths;
			_scd.Scan(&filePaths);

			for (auto& filePath : filePaths)
			{
				std::string identifier = getFileIdentifier(filePath);
				//Determine whether the file exists in memory
				if (!_dma.Exists(filePath))
				{
					BackUpFileInfo->push_back(make_pair(filePath, identifier));
					continue;
				}
				//Judge whether the file content has been modified
				std::string oldIdentifier;
				_dma.Get(filePath, &oldIdentifier);
				if (identifier == oldIdentifier)
				{
					continue;
				}
				BackUpFileInfo->push_back(make_pair(filePath, identifier));
			}
			return true;
		}

		//upload file
		bool Upload(const std::string& filePath)
		{
			httplib::MultipartFormDataItems items;
			httplib::MultipartFormData file1;
			file1.name = "file";
			file1.filename = fs::path(filePath).filename().string();	//file name
			file1.content_type = "application/octet-stream";
			bool ret = Util::FileRead(filePath, &file1.content);
			if (!ret)
			{
				std::cout << " Util::FileRead(filePath, &file1.content); failed" << std::endl;
				return false;
			}
			items.push_back(file1);
			auto rsp = _cli->Post("/multipart", items);
			if (rsp && rsp->status == 200)
			{
				return true;
			}
			else if(rsp && rsp->status == 206)
			{
				std::cout << "服务器要求进行断点续传" << std::endl;
			}
			return false;
		}

		//start to run client
		bool Run()
		{
			while (1)
			{
				//1.read config file
				_dma.Read();
				//2.Get the file information that needs to be backed up
				std::vector<std::pair<std::string, std::string>> arr;
				this->Scan(&arr);

				//3.Traverse the file information to be backed up
				//and upload the files one by one
				for (const auto& file : arr)
				{
					std::cout << file.first << " neet to back up!" << std::endl;
					//upload file to server
					bool ret = this->Upload(file.first);
					//This round of upload failed. 
					//Inconsistency will be detected in the next round of upload.
					//Continue to upload
					if (!ret)
					{
						std::cout << "\tfile  " << file.first << " upload to server failed!" << std::endl;
						continue;
					}
					else
					{
						std::cout << "\tfile  " << file.first << " upload to server successfully!" << std::endl;
					}
					//Record backed up file information
					_dma.Modify(file.first, file.second);
					//4.Write data to configuration file
					_dma.Write();
				}
#ifdef _WIN32
				Sleep(1000);
#else
				sleep(1);
#endif
			}
			return true;
		}

	private:
		//Gets the unique identification of the file
		std::string getFileIdentifier(const std::string& filePath)
		{
			uint64_t fileSz = fs::file_size(filePath);	//file size
			auto time_info = fs::last_write_time(filePath);	//the last modify time of file
			std::time_t time = decltype(time_info)::clock::to_time_t(time_info);

			std::string identifier;
			//The file identifier consists of file size and file time
			identifier = std::to_string(fileSz) + std::asctime(std::localtime(&time));
			
			//进行特殊处理，将范围不是[0,9],[A,Z],[a,z]的字符替换为A
			for (auto& ch : identifier)
			{
				if ((ch >= '0' && ch <= '9') || (ch >= 'A' && ch <= 'Z')
					|| (ch >= 'a' && ch <= 'z'))
				{
					continue;
				}
				ch = 'A';
			}

			return identifier;
		}
	};	//end of Client
}	//end of namespace CloudBackup
