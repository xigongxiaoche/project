#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <unordered_map>
#include <experimental/filesystem>
#include <stdint.h>
#ifdef _WIN32
#include <Windows.h>
#else 
#include <unistd.h>
#endif
#include "httplib.h"
#include <utility>
#include "bundle.h"
#include <time.h>
#include <pthread.h>

namespace CloudBackup
{
	namespace fs = std::experimental::filesystem;	//c++17 filesystem

	//directory scan
	class ScanDir{
		private:
			std::string _path;	//directory route
		public:
			ScanDir(const std::string& path)
				:_path(path)
			{
				//Judge whether the directory exists. 
				//If it does not exist, create a new directory
				if (!fs::exists(_path))
				{
					//Directory hierarchies can also be created
					fs::create_directories(fs::path(_path));
				}
				//Ensure that the directory ends with /
				if(_path.back() != '/')
				{
					_path += '/'; 
				}
			}

			//Directory traversal lists the pathnames of all file names.
			//The depth of directory scanning is 1
			bool Scan(std::vector<std::string>* filePaths)
			{
				for(auto& file : fs::directory_iterator(_path))
				{
					std::string name;
					name = file.path().filename().string();
					//Skip directory
					if (fs::is_directory(file.path()))
					{
						continue;
					}
					//Insert file path into result set
					filePaths->push_back(_path + name); 
				}
				return true;	
			}
	}; //end of ScanDir

	//Tool interface
	class Util{
		public:
			//read content of file
			static bool FileRead(const std::string& file, std::string* body)
			{
				pthread_rwlock_t g_rwlock; //读写锁
				//初始化读写锁
				pthread_rwlock_init(&g_rwlock, NULL);
				pthread_rwlock_rdlock(&g_rwlock);	//加读锁

				std::ifstream infile;
				infile.open(file, std::ios::binary);
				if(!infile.is_open())
				{
					pthread_rwlock_unlock(&g_rwlock);	
					pthread_rwlock_destroy(&g_rwlock);//销毁读写锁  
					std::cout << "open file failed int read file!" << std::endl;  
					return false;
				}
				//get size of file 
				uint64_t len = fs::file_size(file);
				//Capacity must be increased, 
				//otherwise it is invalid to insert data into an empty container
				body->clear();
				body->resize(len);
				//read data of file
				infile.read(&((*body)[0]), len);
				if(!infile.good())
				{
					pthread_rwlock_unlock(&g_rwlock);	
					pthread_rwlock_destroy(&g_rwlock);//销毁读写锁  
					std::cout << "read data failed int read file!" << std::endl;  
					return false;
				}
				//close file
				infile.close();
				pthread_rwlock_unlock(&g_rwlock);
				pthread_rwlock_destroy(&g_rwlock);//销毁读写锁  
				return true;
			}

			//Interval reading
			static bool RangeRead(const std::string& file, std::string* body, int start, int end)
			{
				pthread_rwlock_t g_rwlock; //读写锁
				//初始化读写锁
				pthread_rwlock_init(&g_rwlock, NULL);
				pthread_rwlock_rdlock(&g_rwlock);	//加读锁

				std::ifstream infile;
				infile.open(file, std::ios::binary);
				if(!infile.is_open())
				{
					pthread_rwlock_unlock(&g_rwlock);	
					pthread_rwlock_destroy(&g_rwlock);//销毁读写锁  
					std::cout << "open file failed int read file!" << std::endl;  
					return false;
				}

				//get size of file 
				uint64_t len = fs::file_size(file);
				if(end == -1)
				{
					end = len - 1;
				}
				//get range size 
				uint64_t steps = end - start + 1;
				std::cout << "RangeRead range size: " << steps << std::endl;

				body->clear();
				body->resize(steps);
				//File pointer jump position
				infile.seekg(start, std::ios_base::beg);
				infile.read(&((*body)[0]), steps); 
				if(!infile.good())
				{	
					std::cout << "RangeRead file read failed!" << std::endl;
					infile.close();
					pthread_rwlock_unlock(&g_rwlock);	
					pthread_rwlock_destroy(&g_rwlock);//销毁读写锁  
					return false;
				}
				infile.close();
				pthread_rwlock_unlock(&g_rwlock);	
				pthread_rwlock_destroy(&g_rwlock);//销毁读写锁  
				return true;
			}

			//write data into file
			static bool FileWrite(const std::string& file, std::string& body)
			{
				pthread_rwlock_t g_rwlock; //读写锁
				//初始化读写锁
				pthread_rwlock_init(&g_rwlock, NULL);
				pthread_rwlock_wrlock(&g_rwlock);		//加写锁

				std::ofstream ofs;
				ofs.open(file, std::ios::binary);
				if(!ofs.is_open())
				{
					pthread_rwlock_unlock(&g_rwlock);	
					pthread_rwlock_destroy(&g_rwlock);//销毁读写锁  
					std::cout << "open file failed in write file!" << std::endl;  
					return false;
				}
				ofs.write(&body[0], body.size());
				if(!ofs.good())
				{
					std::cout << "write data failed in read file!" << std::endl;  
					ofs.close();
					pthread_rwlock_unlock(&g_rwlock);	
					pthread_rwlock_destroy(&g_rwlock);//销毁读写锁  
					return false;
				}
				ofs.close();
				pthread_rwlock_unlock(&g_rwlock);	
				pthread_rwlock_destroy(&g_rwlock);//销毁读写锁  
				return true;
			}

			//cut words
			static int Split(const std::string& content, const std::string& spacer,
					std::vector<std::string>* SplitRes)
			{
				int cnt = 0;
				size_t start = 0, pos = 0;
				while(start < content.size())
				{
					pos = content.find(spacer, start);
					if(pos == std::string::npos)
					{
						break;
					}

					std::string tmp = content.substr(start, pos - start);
					SplitRes->push_back(tmp);
					start = pos + spacer.size();
					++cnt;
				}
				if(start < content.size())
				{
					std::string tmp = content.substr(start);
					SplitRes->push_back(tmp);
					++cnt;
				}
				return cnt;
			}
	};	//end of Util



	//manage data
	class DataManager
	{
		private:
			std::string _path;	//path of config file
			std::unordered_map<std::string, std::string> _map;	//key infomation of file
			pthread_rwlock_t _rwlock; //读写锁
		public:
			DataManager(const std::string& path)
				:_path(path)	
			{
				pthread_rwlock_init(&_rwlock, NULL);//初始化一个读写锁  
			}
			~DataManager()
			{
				pthread_rwlock_destroy(&_rwlock);//销毁读写锁
			}

			//Reads data from a file into a hash table in memory
			bool Read()
			{
				std::string content;
				bool ret = Util::FileRead(_path, &content);
				if(!ret)
				{
					std::cout << "read data failed from file to unordered_map!" << std::endl; 
					return false;
				}

				std::vector<std::string> content_v;
				Util::Split(content, "\5", &content_v);
				for(const auto& fileInfo : content_v)
				{
					std::vector<std::string> file_v;
					Util::Split(fileInfo, "=", &file_v);
					pthread_rwlock_wrlock(&_rwlock);	//加写锁
					_map[file_v[0]] = file_v[1];
					pthread_rwlock_unlock(&_rwlock);
				}
				return true;
			}

			//Writes data from a hash table in memory to a file
			bool Write()
			{
				std::string content;
				pthread_rwlock_rdlock(&_rwlock);	//加读锁
				for(const auto& kv : _map)
				{
					content += kv.first + "=" + kv.second + "\5";
				}
				pthread_rwlock_unlock(&_rwlock);

				if(!Util::FileWrite(_path, content))
				{
					std::cout << "write data failed from memory to file!" << std::endl;
					return false;
				}
				return true;
			}

			//Add data to a hash table in memory
			bool Add(const std::string& key, const std::string& val)
			{
				if(Exists(key))
				{
					std::cout << "Add data failed into hashTale of memory!" << std::endl; 
					return false;
				}
				pthread_rwlock_wrlock(&_rwlock);	//加写锁
				_map[key] = val;
				pthread_rwlock_unlock(&_rwlock);	
				return true;
			}

			//Delete data from hash table in memory
			bool Delete(const std::string& key)
			{
				if(!Exists(key))
				{
					std::cout << "key not exists in HashTable!" << std::endl; 
					return false;
				}
				pthread_rwlock_wrlock(&_rwlock);	//加写锁
				_map.erase(key);
				pthread_rwlock_unlock(&_rwlock);	
				return true;
			}

			//Modify data of hash table in memory
			//if key not exists ,create a kv 
			bool Modify(const std::string& key, const std::string& val)
			{
				pthread_rwlock_wrlock(&_rwlock);	//加写锁
				_map[key] = val;
				pthread_rwlock_unlock(&_rwlock);
				return true;
			}

			//Determine whether the hash table in memory exists
			bool Exists(const std::string& key)
			{
				pthread_rwlock_rdlock(&_rwlock);	//加读锁
				if(_map.count(key))
				{
					pthread_rwlock_unlock(&_rwlock);	
					return true;
				}
				pthread_rwlock_unlock(&_rwlock);	
				return false;
			}

			//Get the value corresponding to the hash table key in memory
			bool Get(const std::string& key, std::string* val)
			{
				if(!Exists(key))
				{
					return false;
				}
				pthread_rwlock_rdlock(&_rwlock);	//加读锁
				*val = _map[key];
				pthread_rwlock_unlock(&_rwlock);	
				return true;
			}

			//Get all files name
			bool GetAllFileNames(std::vector<std::string>* fileNames)
			{
				fileNames->clear();
				pthread_rwlock_rdlock(&_rwlock);	//加读锁
				for(const auto& kv : _map)
				{
					fileNames->push_back(kv.first);
				}
				pthread_rwlock_unlock(&_rwlock);
				return true;
			}

			//判断数据管理器是否为空
			bool Empty()
			{
				pthread_rwlock_rdlock(&_rwlock);	//加读锁
				bool ret = _map.empty();
				pthread_rwlock_unlock(&_rwlock);
				return ret;
			}
	};	//end of DataManager


#define Monitoringdirectory "./MonitorDir"		//Monitoring directory
#define ConfigFile "./Config.dat"				//config file
	//client
	class Client
	{
		private:
			ScanDir _scd;				//Directory scan object
			DataManager _dma;			//Data management object
			httplib::Client* _cli;		//httplib client

		public:
			Client(const std::string& host, int port)
				:_scd(Monitoringdirectory)
				 , _dma(ConfigFile)
				 , _cli(new httplib::Client(host, port))
		{}

			//Get the file information that needs to be backed up
			//Backupfileinfo: output type parameter
			//The first of each member is the file path,
			//and the second is the unique identifier of the file	
			bool Scan(std::vector<std::pair<std::string, std::string>>* BackUpFileInfo)
			{
				//List all file paths in the directory
				std::vector<std::string> filePaths;
				_scd.Scan(&filePaths);

				for (auto& filePath : filePaths)
				{
					std::string identifier = getFileIdentifier(filePath);
					//Determine whether the file exists in memory
					if (!_dma.Exists(filePath))
					{
						BackUpFileInfo->push_back(make_pair(filePath, identifier));
						continue;
					}
					//Judge whether the file content has been modified
					std::string oldIdentifier;
					_dma.Get(filePath, &oldIdentifier);
					if (identifier == oldIdentifier) 
					{
						continue;
					}
					BackUpFileInfo->push_back(make_pair(filePath, identifier));
				}
				return true;
			}

			//upload file
			bool Upload(const std::string& filePath)
			{
				httplib::MultipartFormDataItems items;
				httplib::MultipartFormData file1;
				file1.name = "file";
				file1.filename = fs::path(filePath).filename().string();	//file name
				file1.content_type = "application/octet-stream";
				bool ret = Util::FileRead(filePath, &file1.content);
				if (!ret)
				{
					std::cout << " Util::FileRead(filePath, &file1.content);" << std::endl;
					return false; 
				}
				items.push_back(file1);
				auto rsp = _cli->Post("/multipart", items);
				if (rsp && rsp->status == 200)
				{
					return true;
				}
				return false;
			}

			//start to run client
			bool Run()
			{	
				while (1)
				{
					//1.read config file
					_dma.Read();
					//2.Get the file information that needs to be backed up
					std::vector<std::pair<std::string, std::string>> arr;
					this->Scan(&arr);

					//3.Traverse the file information to be backed up
					//and upload the files one by one
					for (const auto& file : arr)
					{
						std::cout << file.first << " neet to back up!" << std::endl;
						//upload file to server
						bool ret = this->Upload(file.first);
						//This round of upload failed. 
						//Inconsistency will be detected in the next round of upload.
						//Continue to upload
						if (!ret)
						{
							continue;
						}
						std::cout << "\tfile  " << file.first << "upload to server successfully!" << std::endl;
						//Record backed up file information
						_dma.Modify(file.first, file.second);
						//4.Write data to configuration file
						_dma.Write();
					}
#ifdef _WIN32
					Sleep(1000);
#else 
					sleep(1);
#endif
				}
				return true;
			}

		private:
			//Gets the unique identification of the file
			std::string getFileIdentifier(const std::string& filePath)
			{
				uint64_t fileSz = fs::file_size(filePath);	//file size
				auto time_info = fs::last_write_time(filePath);	//the last modify time of file
				std::time_t time = decltype(time_info)::clock::to_time_t(time_info);

				std::string identifier;
				//The file identifier consists of file size and file time
				identifier = std::to_string(fileSz) + std::asctime(std::localtime(&time));

				//进行特殊处理，将范围不是[0,9],[A,Z],[a,z]的字符替换为A
				for(auto& ch : identifier)
				{
					if((ch >= '0' && ch <= '9') || (ch >= 'A' && ch <= 'Z') 
							|| (ch >= 'a' && ch <= 'z'))
					{	
						continue;
					}
					ch = 'A';
				}

				return identifier;
			}
	};	//end of Client

	//Compress and UnCompress
	class Compress{
		public:
			//Compress
			static bool Press(const std::string& filePath, std::string* res)
			{
				std::cout << "Into Press" << std::endl;
				if(!res)
				{
					std::cout << "res is nullptr!" << std::endl;
					return false;
				}

				std::string origin;
				bool ret = Util::FileRead(filePath, &origin);
				if(!ret)
				{	
					std::cout << "FileRead failed" << std::endl;
					return false;
				}
				std::cout << "start to pack " << std::endl;
				std::cout << "src file size: " << origin.size() << std::endl;

				pthread_rwlock_t g_rwlock; //读写锁
				//初始化读写锁
				pthread_rwlock_init(&g_rwlock, NULL);
				pthread_rwlock_wrlock(&g_rwlock);               //加写锁
				*res = bundle::pack(bundle::LZIP, origin);
				std::cout << "after Compress, fileSz : " << res->size() << std::endl;
				pthread_rwlock_unlock(&g_rwlock);
				pthread_rwlock_destroy(&g_rwlock);
				std::cout << "Press successfully!" << std::endl;

				return true;			
			}

			//UnCompress
			static bool UnPress(const std::string& filePath, std::string* res)
			{
				if(!res)
				{
					return false;
				}

				std::string origin;
				bool ret = Util::FileRead(filePath, &origin);
				if(!ret)
				{	
					return false;
				}
				pthread_rwlock_t g_rwlock; //读写锁
				//初始化读写锁
				pthread_rwlock_init(&g_rwlock, NULL);
				pthread_rwlock_wrlock(&g_rwlock);               //加写锁
				*res = bundle::unpack(origin);
				pthread_rwlock_unlock(&g_rwlock);
				pthread_rwlock_destroy(&g_rwlock);
				std::cout << "Press successfully!" << std::endl;
				return true;
			} 
	};//end of Comprss

#define BACKUPDIR "./Backup/"			//Backup file storage directory
#define BACKCONFIGFILENAME "back.conf"		//backup config file name

	DataManager g_backDma(BACKCONFIGFILENAME);		//backup config manager

	class Server
	{
		private:
			httplib::Server _srv;
		private:
			//Gets the unique identification of the file
			static std::string getFileIdentifier(const std::string& filePath)
			{
				uint64_t fileSz = fs::file_size(filePath);	//file size
				auto time_info = fs::last_write_time(filePath);	//the last modify time of file
				std::time_t time = decltype(time_info)::clock::to_time_t(time_info);

				std::string identifier;
				//The file identifier consists of file size and file time
				identifier = std::to_string(fileSz) + std::asctime(std::localtime(&time));

				//进行特殊处理，将范围不是[0,9],[A,Z],[a,z]的字符替换为A
				for(auto& ch : identifier)
				{
					if((ch >= '0' && ch <= '9') || (ch >= 'A' && ch <= 'Z') 
							|| (ch >= 'a' && ch <= 'z'))
					{	
						continue;
					}
					ch = 'A';
				}
				return identifier;
			}

		public:
			//upload file
			static void Upload(const httplib::Request& req, httplib::Response& rsp)
			{
				//Determine whether the file exists
				bool ret = req.has_file("file");
				if(!ret)
				{
					std::cout << "upload file not exists!" << std::endl;
					rsp.status = 400;
					return;
				}

				//The backup directory does not exist. New directory
				if(!fs::exists(BACKUPDIR))
				{
					fs::create_directories(fs::path(BACKUPDIR));
				}

				//Get file content
				auto file = req.get_file_value("file");
				std::string filename = BACKUPDIR + file.filename;
				ret = Util::FileWrite(filename, file.content);
				if(!ret)
				{
					std::cout << "FileWrite failed, file upload failed" << std::endl;
					rsp.status = 500;
					return;
				}

				//update backup data manager
				g_backDma.Modify(filename, filename);
				//write data into config file
				g_backDma.Write();
				std::cout << "file upload successfully" << std::endl;
				rsp.status = 200;
				return;
			}

			//download file
			static void Download(const httplib::Request& req, httplib::Response& rsp)
			{
				std::cout << "into download" << std::endl;
				std::string filePath = req.matches[1];
				filePath = "./" + filePath;
				std::cout << "read filePath : " << filePath << std::endl;
				std::string realPath;	//文件实际存储路径
				g_backDma.Get(filePath, &realPath);
				std::string tag = getFileIdentifier(realPath);
				std::cout << "tag : " << tag << std::endl;
				
				if(g_backDma.Exists(filePath))
				{
					std::cout << "下载的文件存在" << std::endl;

					//需要解压缩
					if(filePath != realPath)
					{	
						std::cout << "需要解压缩" << std::endl;
						std::string body;
						Compress::UnPress(realPath, &body);
						//将解压缩后的结果写入文件
						Util::FileWrite(filePath, body);
						//更新数据管理器和配置文件
						g_backDma.Modify(filePath, filePath);
						g_backDma.Write();
						//删除源文件
						unlink(realPath.c_str());
					}
				}	
				else
				{
					std::cout << "下载的文件不存在" << std::endl;
				}

				if(req.has_header("If-Range"))
				{
					std::string oldETag = req.get_header_value("If-Range");
					if(oldETag == tag)
					{
						std::cout << "进行断点续传" << std::endl;
						Util::FileRead(filePath, &rsp.body);
						rsp.status = 206;
						return;
					}
					else
					{
						std::cout << "old ETag != new ETag" << std::endl;
					}
				}

				bool ret = Util::FileRead(filePath, &rsp.body);
				if(!ret)
				{
					rsp.status = 500;
					std::cout << "data into rsp Content failed" << std::endl;
					return;
				}
				rsp.set_header("Content-Type", "application/octet-stream");
				rsp.set_header("Accept-Ranges", "bytes");
				rsp.set_header("ETag", tag);
				rsp.status = 200;				
				std::cout << "data into rsp Content successfully" << std::endl;
			}

			//show all file
			static void ListAll(const httplib::Request& req, httplib::Response& rsp)
			{
				//get file name from backup config datamanger 
				std::vector<std::string> showFileNames;
				g_backDma.GetAllFileNames(&showFileNames);

				//organize response
				std::stringstream ss;
				ss << "<html><head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>";
				ss << "<title>晓鹏云</title>";
				ss << "<style> body{ background-image:url('./cuteGirl.jpg'); background-size: cover;}</style>";

				ss << "</head><body>";
				ss << "<h1>file backup: </h1>";
				for(auto& filename : showFileNames)
				{
					fs::path fpath(filename);
					ss << "<a href = '/Download/" << filename << "'>" << fpath.filename().string() << "</a>";
					ss << "<hr/>";
				}
				ss << "</body></html>";

				rsp.body = ss.str();
				rsp.set_header("Content-Type", "text/html");
				rsp.status = 200;

				return;
			}

			//start to run server
			void Run(int port = 9000)
			{
				std::cout << "进入服务器模块" << std::endl;
				//read config file
				g_backDma.Read();

				_srv.Post("/multipart", Upload);
				_srv.Get("/ListAll", ListAll);
				_srv.Get("/Download/(.*)", Download);	//regular expression
				_srv.listen("0.0.0.0", port);
			}
	};//end of Server


#define PACKDIR "./Pack/"	//pack directory

	//manage backed file and compress the unhot file 
	class FileManager{
		private:
			std::string _packDir;	//pack directory
			ScanDir _scan;		
			time_t _hotTime;	//热点时间
		public:
			FileManager(time_t hotTime = 600)
				:_packDir(PACKDIR)
				,_scan(BACKUPDIR)
				,_hotTime(hotTime)
			{}

			bool Run()
			{
				//std::cout << "into FileManager Run" << std::endl;

				if(!fs::exists(_packDir))
				{
					fs::create_directories(fs::path(_packDir));
				}
				
				while(1)
				{
					//扫描备份目录，得到所有文件名
					std::vector<std::string> filePaths;
					_scan.Scan(&filePaths);
				//	std::cout << "扫描备份目录结束" << std::endl;
					for(const auto& filePath : filePaths)
					{
						time_t oldTime = getLastAccessTime(filePath);
						time_t curTime = time(NULL);
						if((curTime - oldTime) >= _hotTime)
						{
				//			std::cout << "超过热点时间 " << _hotTime << std::endl;
							
							fs::path fpath(filePath);
							std::string packPath = _packDir + fpath.filename().string() + ".pack";

							std::string packContent;
							//压缩非热点文件并存储
							Compress::Press(filePath, &packContent);
				//			std::cout << "压缩非热点文件成功" << std::endl;
							Util::FileWrite(packPath, packContent);
				//			std::cout << "压缩文件存储成功" << std::endl;
							//更新数据管理器和配置文件
							g_backDma.Modify(filePath, packPath);
				//			std::cout << "更新数据管理器成功" << std::endl;
							g_backDma.Write();
				//			std::cout << "更新配置文件成功" << std::endl;
							//删除源文件
							unlink(filePath.c_str());
				//			std::cout << "删除源文件成功" << std::endl;
						}
					}
					//休眠1秒
					sleep(1);
				//	std::cout << "FileManager一轮扫描结束" << std::endl;
				}
				return true;
			}		
		private:
			//文件最后一次访问时间
			time_t getLastAccessTime(std::string filePath)
			{
				struct stat st;
				stat(filePath.c_str(), &st);
				return st.st_atime;				
			}
	};//end of FileManager
}	//end of namespace CloudBackup
