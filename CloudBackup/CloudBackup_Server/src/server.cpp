#include "Cloud.hpp"
#include <thread>
#include <unistd.h>

void FileManage()
{
	CloudBackup::FileManager fm(600);		//设置热点时间为10min
	fm.Run();
}

int main()
{
	std::thread t(FileManage);
	t.detach();

	CloudBackup::Server srv;
	srv.Run();

	return 0;
}
