#include "httplib.h"
#include <fstream>
#include <string>
#include <experimental/filesystem>
#include <iostream>

namespace fs = std::experimental::filesystem;

bool Read(const std::string& filePath, std::string* body)
{
	std::ifstream ifs;
	ifs.open(filePath);
	if(!ifs.is_open())
	{
		std::cout << filePath << " open failed!" << std::endl;
		return false;
	}

	uint64_t fileSz = fs::file_size(filePath);	
	body->clear();
	body->resize(fileSz);
	ifs.read(&((*body)[0]), fileSz);
	if(!ifs.good())
	{
		std::cout << "read data from file failed " << std::endl;
		return false;
	}
	ifs.close();

	return true;
}

bool RangeRead(const std::string& file, std::string* body, int start, int end)
{
	std::ifstream infile;
	infile.open(file, std::ios::binary);
	if(!infile.is_open())
	{
		std::cout << "open file failed in read file!" << std::endl;
		return false;
	}

	//get size of file 
	uint64_t len = fs::file_size("./test.mp4");
	
	//get range size 
	uint64_t steps = end - start + 1;

	body->clear();
	body->resize(steps);
	//File pointer jump position
	infile.seekg(start, std::ios_base::beg);
	infile.read(&((*body)[0]), steps);
	if(!infile.good())
	{
		std::cout << "RangeRead file read failed!" << std::endl;
		infile.close();
		return false;
	}
	infile.close();
	return true;
}

//断点续传
void Download(const httplib::Request& req, httplib::Response& rsp)
{
	std::string filePath = "./test.mp4";
	std::string etag = "grwhjigwpgwpjgwojegpojepowjg";
	if(req.has_header("If-Range"))
	{
		std::string oldEtag = "grwhjigwpgwpjgwojegpojepowjg";
		if(etag == oldEtag)
		{
			std::cout << "断点续传" << std::endl;
			Read(filePath, &rsp.body);
			rsp.status = 206;
			return;
		}
	}
	bool ret = Read(filePath, &rsp.body);
	if(!ret)
	{
		rsp.status = 500;
		std::cout << "file data read into rsp body failed" << std::endl;
		return;
	}
	rsp.set_header("Content-Type", "application/octet-stream");
	rsp.set_header("Accept-Ranges", "bytes");	
	rsp.set_header("ETag", etag.c_str());
	rsp.status = 200;
	for(auto& head : rsp.headers)
	{
		std::cout << head.first << " : " << head.second << std::endl;
	}
	
	std::cout << "file data read into rsp body successfully" << std::endl;
}

int main()
{
	std::cout << "main start" << std::endl;
	httplib::Server srv;
	srv.Get("/download", Download);
	srv.listen("0.0.0.0", 9000);	
	std::cout << "main end" << std::endl;

	return 0;
}
